package com.blog.filters;


import com.blog.model.Role;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "adminActionsFilter",
urlPatterns = {"/switchUserStatus"})
public class AdminActionsFilter implements Filter {
    private FilterConfig filterConfig;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest)request;
        HttpServletResponse httpServletResponse = (HttpServletResponse)response;
        HttpSession session = httpServletRequest.getSession();

        if (session == null || session.getAttribute("userRole") != Role.Admin){
            httpServletResponse.sendRedirect("articles?page=1");
        }else{
            chain.doFilter(httpServletRequest, httpServletResponse);
        }


    }

    @Override
    public void destroy() {

    }
}
