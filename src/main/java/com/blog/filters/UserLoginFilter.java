package com.blog.filters;

import com.blog.service.user.UserService;
import com.blog.service.user.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebFilter(filterName = "userLoginFilter",
    urlPatterns = "/login")
public class UserLoginFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest)request;
        HttpServletResponse httpServletResponse = (HttpServletResponse)response;

        String login = (String)request.getParameter("login");
        UserService userService = new UserServiceImpl();

        try {
            if (login != null && userService.userExists(login) && !userService.isActive(login)) {
                httpServletRequest.setAttribute("message", "You are banned and can't log in!");
                httpServletRequest.getRequestDispatcher("login.jsp").forward(httpServletRequest, httpServletResponse);
            }else{
                chain.doFilter(httpServletRequest, httpServletResponse);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {

    }
}
