package com.blog.service.article;

import com.blog.dao.article.ArticleDAO;
import com.blog.dao.article.ArticleDAOImpl;
import com.blog.model.Article;

import java.sql.SQLException;
import java.util.List;

public class ArticleServiceImpl implements ArticleService{
    private ArticleDAO articleDAO = new ArticleDAOImpl();

    @Override
    public Article insertArticle(Article article) throws SQLException {
        return articleDAO.insertArticle(article);
    }

    @Override
    public Article getArticle(Long id) throws SQLException {
        return articleDAO.getArticle(id);
    }

    @Override
    public Article updateArticle(Article article) throws SQLException {
        return articleDAO.updateArticle(article);
    }

    @Override
    public void deleteArticle(Long id) throws SQLException {
        articleDAO.deleteArticle(id);
    }

    @Override
    public List<Article> getAll() throws SQLException {
        return articleDAO.getAll();
    }

    @Override
    public long getNumberOfPublishedArticles() throws SQLException {
        return (long)Math.ceil(articleDAO.getNumberOfPublishedArticles() / 5.0);
    }

    @Override
    public List<Article> getPublishedLimitedArticles(int from, int to) throws SQLException {
        return articleDAO.getPublishedLimitedArticles(from, to);
    }

    @Override
    public List<Article> getArticlesByAuthorId(Long id) throws SQLException {
        return articleDAO.getArticlesByAuthorId(id);
    }

    @Override
    public long getNumberOfPublishedArticlesByTagId(Long id) throws SQLException {
        return (long)Math.ceil(articleDAO.getNumberOfPublishedArticlesByTagId(id) / 5.0);
    }

    @Override
    public List<Article> getPublishedLimitedArticlesByTagId(int from, int to, Long tagId) throws SQLException {
        return articleDAO.getPublishedLimitedArticlesByTagId(from, to, tagId);
    }
}
