package com.blog.service.user;

import com.blog.dao.user.UserDAO;
import com.blog.dao.user.UserDAOImpl;
import com.blog.model.Role;
import com.blog.model.User;
import com.blog.util.DBConnection;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserServiceImpl implements UserService{
    private UserDAO userDAO = new UserDAOImpl();

    private String hashPassword(String plainPassword){
        return BCrypt.hashpw(plainPassword, BCrypt.gensalt());
    }

    private boolean passwordMatches(String plainPassword, String hashedPassword){
        return BCrypt.checkpw(plainPassword, hashedPassword);
    }

    @Override
    public User insertUser(User user) throws SQLException {
        user.setPassword(hashPassword(user.getPassword())); // Hashing the password and setting it in our User object.
        return userDAO.insertUser(user);
    }

    @Override
    public User getUser(Long id) throws SQLException {
        return userDAO.getUserById(id);
    }

    @Override
    public User updateUser(User user) throws SQLException {
        return userDAO.updateUser(user);
    }

    @Override
    public void deleteUser(Long id) throws SQLException {
        userDAO.deleteUser(id);
    }

    @Override
    public List<User> getAll() throws SQLException {
        return userDAO.getAll();
    }

    @Override
    public User authenticateUser(String login, String password) throws SQLException {
        User user = null;
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE login = ?");
        ){

            statement.setString(1, login);

            ResultSet rs = statement.executeQuery();

            if (rs.next() && passwordMatches(password, rs.getString("password"))){
                user = new User();
                user.setId(rs.getLong("id"));
                user.setName(rs.getString("name"));
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setEmail(rs.getString("email"));
                user.setRole(Role.valueOf(rs.getString("role")));
                user.setActive(rs.getBoolean("is_active"));
            }
        }

        return user;
    }

    @Override
    public boolean isActive(String login) throws SQLException {
        return userDAO.isActive(login);
    }

    @Override
    public boolean userExists(String login) throws SQLException {
        return userDAO.userExists(login);
    }

    @Override
    public User getUserByEmail(String email) throws SQLException {
        return userDAO.getUserByEmail(email);
    }

    @Override
    public void switchUserStatusByEmail(String email, boolean status) throws SQLException {
        userDAO.switchUserStatusByEmail(email, status);
    }

    @Override
    public long getNumberOfUsers() throws SQLException {
        return (long)Math.ceil(userDAO.getNumberOfUsers() / 10.0);
    }

    @Override
    public List<User> getLimitedUsers(int from, int to) throws SQLException {
        return userDAO.getLimitedUsers(from, to);
    }
}
