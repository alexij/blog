package com.blog.service.user;

import com.blog.model.User;

import java.sql.SQLException;
import java.util.List;

public interface UserService {
    User insertUser(User user) throws SQLException;
    User getUser(Long id) throws SQLException;
    User updateUser(User user) throws  SQLException;
    void deleteUser(Long id) throws SQLException;
    List<User> getAll() throws SQLException;
    User getUserByEmail(String email) throws SQLException;
    User authenticateUser(String login, String password) throws SQLException;
    boolean isActive(String login) throws SQLException;
    boolean userExists(String login) throws SQLException;
    long getNumberOfUsers() throws SQLException;
    List<User> getLimitedUsers(int from, int to) throws SQLException;
    void switchUserStatusByEmail(String email, boolean status) throws SQLException;
}
