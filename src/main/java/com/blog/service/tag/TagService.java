package com.blog.service.tag;

import com.blog.model.Tag;

import java.sql.SQLException;
import java.util.List;

public interface TagService {
    Tag insertTag(Tag tag) throws SQLException;
    Tag getTag(Long id) throws SQLException;
    Tag updateTag(Tag tag) throws  SQLException;
    void deleteTag(Long id) throws SQLException;
    List<Tag> getAll() throws SQLException;
}
