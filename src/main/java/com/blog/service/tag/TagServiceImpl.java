package com.blog.service.tag;

import com.blog.dao.tag.TagDAO;
import com.blog.dao.tag.TagDAOImpl;
import com.blog.model.Tag;

import java.sql.SQLException;
import java.util.List;

public class TagServiceImpl implements TagService{
    private TagDAO tagDAO = new TagDAOImpl();

    @Override
    public Tag insertTag(Tag tag) throws SQLException {
        return tagDAO.insertTag(tag);
    }

    @Override
    public Tag getTag(Long id) throws SQLException {
        return tagDAO.getTag(id);
    }

    @Override
    public Tag updateTag(Tag tag) throws SQLException {
        return tagDAO.updateTag(tag);
    }

    @Override
    public void deleteTag(Long id) throws SQLException {
        tagDAO.deleteTag(id);
    }

    @Override
    public List<Tag> getAll() throws SQLException {
        return tagDAO.getAll();
    }
}
