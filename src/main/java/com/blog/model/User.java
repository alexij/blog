package com.blog.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class User {
    private Long id;

    @Pattern(regexp = "^[A-Za-z]([A-Za-z0-9_]\\s?){5,29}$",
            message = "Wrong username! Username must contain from 6 to 30 characters.\n" +
                      "It must consist only of English alphanumeric characters, underscores and one whitespace " +
                      "between each alphanumeric and underscore part.\n" +
                      "It cannot start with a digit.")
    @NotNull(message = "The username cannot be null!")
    private String name;

    @Pattern(regexp = "^[A-Za-z]([A-Za-z0-9_]){5,29}$",
            message = "Wrong login! Login must contain from 6 to 30 characters.\n" +
                      "It must consist only of English alphanumeric characters and underscores.\n" +
                      "It cannot start with a digit.")
    @NotNull(message = "The login cannot be null!")
    private String login;

    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,32}$",
            message = "Wrong password! Your password must match the following rules:\n" +
                    "A digit must occur at least once.\n" +
                    "A lower case letter must occur at least once.\n" +
                    "An upper case letter must occur at least once.\n" +
                    "A special character must occur at least once.\n" +
                    "No whitespace allowed in the entire string.\n" +
                    "Password must contain from 6 to 32 characters.")
    @NotNull(message = "Password cannot be null.")
    private String password;

    @Pattern(regexp = "^([\\w]\\.?){6,30}@[\\w]+(\\.[\\w]+)*(\\.[a-z]{2,})$",
            message = "Wrong email! It must match the following rules:\n" +
                      "First part of email must contain from 6 to 30 alphanumeric characters.\n" +
                      "Period is allowed (but not compulsory) once between other characters.\n" +
                      "Email must contain one '@' character.\n" +
                      "In the part after '@' character, there has to be at least 1 character and a dot after it. \n" +
                      "There has to be at least 2 characters after each period.")
    @NotNull(message = "The email cannot be null!")
    private String email;

    @NotNull(message = "Role cannot be null!")
    private Role role;

    @NotNull
    private boolean active;

    public User() {
    }

    public User(String name, String login, String password, String email, Role role, boolean active) {
        this.name = name;
        this.login = login;
        this.password = password;
        this.email = email;
        this.role = role;
        this.active = active;
    }

    public User(Long id, String name, String login, String password, String email, Role role) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.password = password;
        this.email = email;
        this.role = role;
    }

    public User(Long id, String name, String login, String password, String email, Role role, boolean active) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.password = password;
        this.email = email;
        this.role = role;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", role=" + role +
                ", active=" + active +
                "}\n";
    }
}
