package com.blog.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Set;

public class Article {
    private Long id;

    @NotNull(message = "Article name cannot be null!")
    @Size(max = 70, message = "Article name can contain up to 70 characters.")
    private String name;

    @NotNull(message = "Article description cannot be null!")
    @Size(max = 240, message = "Article description can contain up to 240 characters.")
    private String description;

    private String imageURL;
    private LocalDate publishDate;
    private LocalDate updateDate;

    @NotNull
    private User author;
    private String summary;

    private Set<Tag> tags;

    public Article() {
    }

    public Article(Long id, String name, String description, String imageURL, LocalDate publishDate, LocalDate updateDate, User author, String summary) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.imageURL = imageURL;
        this.publishDate = publishDate;
        this.updateDate = updateDate;
        this.author = author;
        this.summary = summary;
    }

    public Article(String name, String description, String imageURL, User author, String summary) {
        this.name = name;
        this.description = description;
        this.imageURL = imageURL;
        this.author = author;
        this.summary = summary;
    }

    public Article(String name, String description, String imageURL, LocalDate publishDate, LocalDate updateDate, User author, String summary) {
        this.name = name;
        this.description = description;
        this.imageURL = imageURL;
        this.publishDate = publishDate;
        this.updateDate = updateDate;
        this.author = author;
        this.summary = summary;
    }

    public Article(String name, String description, String imageURL, LocalDate publishDate, User author, String summary) {
        this.name = name;
        this.description = description;
        this.imageURL = imageURL;
        this.publishDate = publishDate;
        this.author = author;
        this.summary = summary;
    }

    public Article(Long id, String name, String description, String imageURL, LocalDate publishDate, LocalDate updateDate, User author, String summary, Set<Tag> tags) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.imageURL = imageURL;
        this.publishDate = publishDate;
        this.updateDate = updateDate;
        this.author = author;
        this.summary = summary;
        this.tags = tags;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(LocalDate publishDate) {
        this.publishDate = publishDate;
    }

    public LocalDate getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", publishDate=" + publishDate +
                ", updateDate=" + updateDate +
                ", author=" + author +
                ", summary='" + summary + '\'' +
                "}\n";
    }
}
