package com.blog.dao.tag;

import com.blog.model.Tag;
import com.blog.util.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TagDAOImpl implements TagDAO {
    private static final String CREATE_TAG_QUERY = "INSERT INTO tags (name) VALUES (?)";
    private static final String GET_TAG_BY_ID_QUERY = "SELECT * FROM tags WHERE id = ?";
    private static final String UPDATE_TAG_QUERY = "UPDATE tags SET name = ?, password = ? where id = ?";
    private static final String DELETE_TAG_BY_ID_QUERY = "DELETE FROM tags WHERE id = ?";
    private static final String GET_ALL_TAGS_QUERY = "SELECT * FROM tags";

    @Override
    public Tag insertTag(Tag tagToCreate) throws SQLException {
        Tag tag = null;
        try (Connection connection = DBConnection.createConnection(); PreparedStatement createStatement = connection.prepareStatement(CREATE_TAG_QUERY)){
            createStatement.setString(1, tagToCreate.getName());
            createStatement.executeUpdate();
        }

        return tag;
    }

    @Override
    public Tag getTag(Long id) throws SQLException {
        Tag tag = null;

        try (Connection connection = DBConnection.createConnection();
             PreparedStatement getByIdStatement = connection.prepareStatement(GET_TAG_BY_ID_QUERY);
        ) {
            getByIdStatement.setLong(1, id);
            ResultSet rs = getByIdStatement.executeQuery();

            if (rs.next()) {
                tag = new Tag(id, rs.getString("name"));
            }
        }
        return tag;
    }

    @Override
    public Tag updateTag(Tag tag) throws SQLException {
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement updateStatement = connection.prepareStatement(UPDATE_TAG_QUERY);) {
            updateStatement.setString(1, tag.getName());
            updateStatement.executeUpdate();
        }
        return tag;
    }

    @Override
    public void deleteTag(Long id) throws SQLException {
        try(Connection connection = DBConnection.createConnection();
            PreparedStatement deleteByIdStatement = connection.prepareStatement(DELETE_TAG_BY_ID_QUERY);) {

            deleteByIdStatement.setLong(1, id);
            deleteByIdStatement.executeUpdate();
        }
    }

    @Override
    public List<Tag> getAll() throws SQLException {
        List<Tag> list;
        try (Connection connection = DBConnection.createConnection();
             Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(GET_ALL_TAGS_QUERY)) {

            list = new ArrayList<>();
            while (rs.next()) {
                list.add(new Tag(rs.getLong("id"), rs.getString("name")));
            }

        }

        return list;
    }
}
