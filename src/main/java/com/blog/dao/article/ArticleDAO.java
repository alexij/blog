package com.blog.dao.article;

import com.blog.model.Article;
import com.blog.model.Tag;

import java.sql.SQLException;
import java.util.List;

public interface ArticleDAO {
    Article insertArticle(Article article) throws SQLException;
    Article getArticle(Long id) throws SQLException;
    Article updateArticle(Article article) throws  SQLException;
    void deleteArticle(Long id) throws SQLException;
    List<Article> getAll() throws SQLException;
    List<Article> getPublishedLimitedArticles(int from, int to) throws SQLException;
    long getNumberOfPublishedArticles() throws SQLException;
    long getNumberOfPublishedArticlesByTagId(Long id) throws SQLException;
    List<Article> getArticlesByAuthorId(Long id) throws SQLException;
    List<Article> getPublishedLimitedArticlesByTagId(int from, int to, Long tagId) throws SQLException;
}
