package com.blog.dao.article;

import com.blog.dao.tag.TagDAO;
import com.blog.dao.tag.TagDAOImpl;
import com.blog.dao.user.UserDAO;
import com.blog.dao.user.UserDAOImpl;
import com.blog.model.Article;
import com.blog.model.Tag;
import com.blog.util.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ArticleDAOImpl implements ArticleDAO {
    private static final String CREATE_ARTICLE_QUERY = "INSERT INTO articles (name, description, image_url, publish_date, update_date, author_id, summary, tags) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String GET_ARTICLE_BY_ID_QUERY = "SELECT * FROM articles WHERE id = ?";
    private static final String UPDATE_ARTICLE_QUERY = "UPDATE articles SET name = ?, description = ?, image_url = ?, publish_date = ?, update_date = ?, author_id = ?, summary = ? , tags = ? where id = ?";
    private static final String DELETE_ARTICLE_BY_ID_QUERY = "DELETE FROM articles WHERE id = ?";
    private static final String GET_ALL_ARTICLES_DESC_QUERY = "SELECT * FROM articles ORDER BY update_date, id DESC";
    private static final String GET_LIMITED_PUBLISHED_ARTICLES_DESC_QUERY = "SELECT * FROM articles WHERE publish_date IS NOT NULL ORDER BY update_date DESC OFFSET ? LIMIT ?";
    private static final String GET_PUBLISHED_ARTICLES_NUMBER = "SELECT COUNT(id) as recordsAmount FROM articles WHERE publish_date IS NOT NULL";
    private static final String GET_ARTICLES_BY_AUTHOR_ID = "SELECT * FROM articles WHERE author_id = ? ORDER BY update_date, id";
    private static final String GET_LIMITED_PUBLISHED_ARTICLES_BY_TAG_ID_DESC_QUERY = "SELECT * FROM articles WHERE publish_date IS NOT NULL AND ? = ANY(tags) ORDER BY update_date DESC OFFSET ? LIMIT ?";
    private static final String GET_PUBLISHED_ARTICLES_NUMBER_BY_TAG_ID = "SELECT COUNT(id) as recordsAmount FROM articles WHERE publish_date IS NOT NULL AND ? = ANY(tags)";
    @Override
    public Article insertArticle(Article article) throws SQLException {
        List<Long> tagsId = article.getTags().stream().map(Tag::getId).collect(Collectors.toList());

        try (Connection connection = DBConnection.createConnection(); PreparedStatement createArticle = connection.prepareStatement(CREATE_ARTICLE_QUERY)) {
            Array tagsIdArray = connection.createArrayOf("bigint", tagsId.toArray());
            createArticle.setString(1, article.getName());
            createArticle.setString(2, article.getDescription());
            createArticle.setString(3, article.getImageURL());
            if (article.getPublishDate() == null){
                createArticle.setDate(4, null);
                createArticle.setDate(5, null);
            }else {
                createArticle.setDate(4, Date.valueOf(article.getPublishDate()));
                createArticle.setDate(5, Date.valueOf(article.getUpdateDate()));
            }
            createArticle.setLong(6, article.getAuthor().getId());
            createArticle.setString(7, article.getSummary());
            createArticle.setArray(8, tagsIdArray);

            createArticle.executeUpdate();
        }


        return article;
    }

    @Override
    public Article getArticle(Long id) throws SQLException {
        Article article = null;
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement getArticleById = connection.prepareStatement(GET_ARTICLE_BY_ID_QUERY);
             ) {
            getArticleById.setLong(1, id);
            ResultSet rs = getArticleById.executeQuery();

            UserDAO userDAO = new UserDAOImpl();
            TagDAO tagDAO = new TagDAOImpl();
            Set<Tag> tags;
            if (rs.next()) {
                tags = new HashSet<>();
                Long[] tagsId = (Long[]) rs.getArray("tags").getArray();

                for (int i = 0; i < tagsId.length; i++){
                    tags.add(tagDAO.getTag(tagsId[i]));
                }
                article = new Article();
                article.setId(rs.getLong("id"));
                article.setName(rs.getString("name"));
                article.setDescription(rs.getString("description"));
                article.setImageURL(rs.getString("image_url"));
                if (rs.getDate("publish_date") != null) article.setPublishDate(rs.getDate("publish_date").toLocalDate());
                if (rs.getDate("update_date") != null) article.setUpdateDate(rs.getDate("update_date").toLocalDate());
                article.setAuthor(userDAO.getUserById(rs.getLong("author_id")));
                article.setSummary(rs.getString("summary"));
                article.setTags(tags);

            }
        }

        return article;
    }

    @Override
    public Article updateArticle(Article article) throws SQLException {
        List<Long> tagsId = article.getTags().stream().map(Tag::getId).collect(Collectors.toList());
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement updateArticle = connection.prepareStatement(UPDATE_ARTICLE_QUERY);) {
            Array tagsIdArray = connection.createArrayOf("bigint", tagsId.toArray());
            updateArticle.setString(1, article.getName());
            updateArticle.setString(2, article.getDescription());
            updateArticle.setString(3, article.getImageURL());
            if (article.getPublishDate() != null){
                updateArticle.setDate(4, Date.valueOf(article.getPublishDate()));
            }else{
                updateArticle.setDate(4, null);
            }
            if (article.getUpdateDate() != null){
                updateArticle.setDate(5, Date.valueOf(article.getUpdateDate()));
            }else{
                updateArticle.setDate(5, null);
            }
            updateArticle.setLong(6, article.getAuthor().getId());
            updateArticle.setString(7, article.getSummary());
            updateArticle.setArray(8, tagsIdArray);
            updateArticle.setLong(9, article.getId());

            updateArticle.executeUpdate();
        }

        return article;
    }

    @Override
    public void deleteArticle(Long id) throws SQLException {
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement deleteArticleById = connection.prepareStatement(DELETE_ARTICLE_BY_ID_QUERY)) {
            deleteArticleById.setLong(1, id);
            deleteArticleById.executeUpdate();
        }
    }

    @Override
    public synchronized List<Article> getAll() throws SQLException {
        List<Article> list;
        try (Connection connection = DBConnection.createConnection();
             Statement getAllStatement = connection.createStatement();
             ResultSet rs = getAllStatement.executeQuery(GET_ALL_ARTICLES_DESC_QUERY)) {

            UserDAO userDAO = new UserDAOImpl();
            list = new ArrayList<>();
            while (rs.next()) {
                list.add(new Article(rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getString("image_url"),
                        rs.getDate("publish_date").toLocalDate(),
                        rs.getDate("update_date").toLocalDate(),
                        userDAO.getUserById(rs.getLong("author_id")),
                        rs.getString("summary")));
            }
        }


        return list;
    }

    @Override
    public synchronized List<Article> getPublishedLimitedArticles(int start, int total) throws SQLException {
        List<Article> list;
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement statement = connection.prepareStatement(GET_LIMITED_PUBLISHED_ARTICLES_DESC_QUERY)) {

            statement.setInt(1, (start-1));
            statement.setInt(2, total);
            ResultSet rs = statement.executeQuery();
            UserDAO userDAO = new UserDAOImpl();
            list = new ArrayList<>();
            TagDAO tagDAO = new TagDAOImpl();
            Set<Tag> tags;
            while (rs.next()) {
                tags = new HashSet<>();
                Long[] tagsId = null;
                if (rs.getArray("tags") != null) tagsId = (Long[]) rs.getArray("tags").getArray();

                if (tagsId != null) {
                    for (int i = 0; i < tagsId.length; i++) {
                        tags.add(tagDAO.getTag(tagsId[i]));
                    }
                }

                Article article = new Article();
                article.setId(rs.getLong("id"));
                article.setName(rs.getString("name"));
                article.setDescription(rs.getString("description"));
                article.setImageURL(rs.getString("image_url"));
                if (rs.getDate("publish_date") != null) article.setPublishDate(rs.getDate("publish_date").toLocalDate());
                if (rs.getDate("update_date") != null) article.setUpdateDate(rs.getDate("update_date").toLocalDate());
                article.setAuthor(userDAO.getUserById(rs.getLong("author_id")));
                article.setSummary(rs.getString("summary"));
                article.setTags(tags);

                list.add(article);
            }
        }



        return list;
    }

    @Override
    public long getNumberOfPublishedArticles() throws SQLException {
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement getNumberOfPublishedArticlesStatement = connection.prepareStatement(GET_PUBLISHED_ARTICLES_NUMBER);
             ResultSet rs = getNumberOfPublishedArticlesStatement.executeQuery()) {

            if (rs.next()) {
                return rs.getLong("recordsAmount");

            }
        }

        return 0;
    }

    @Override
    public List<Article> getArticlesByAuthorId(Long id) throws SQLException {
        List<Article> list;
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ARTICLES_BY_AUTHOR_ID)) {

            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            UserDAO userDAO = new UserDAOImpl();
            list = new ArrayList<>();
            TagDAO tagDAO = new TagDAOImpl();
            Set<Tag> tags;
            while (rs.next()) {
                tags = new HashSet<>();
                Long[] tagsId = null;
                if (rs.getArray("tags") != null) tagsId = (Long[]) rs.getArray("tags").getArray();

                if (tagsId != null) {
                    for (int i = 0; i < tagsId.length; i++) {
                        tags.add(tagDAO.getTag(tagsId[i]));
                    }
                }

                Article article = new Article();
                article.setId(rs.getLong("id"));
                article.setName(rs.getString("name"));
                article.setDescription(rs.getString("description"));
                article.setImageURL(rs.getString("image_url"));
                if (rs.getDate("publish_date") != null) article.setPublishDate(rs.getDate("publish_date").toLocalDate());
                if (rs.getDate("update_date") != null) article.setUpdateDate(rs.getDate("update_date").toLocalDate());
                article.setAuthor(userDAO.getUserById(rs.getLong("author_id")));
                article.setSummary(rs.getString("summary"));
                article.setTags(tags);


                list.add(article);
            }
        }

        return list;
    }

    @Override
    public List<Article> getPublishedLimitedArticlesByTagId(int start, int total, Long tagId) throws SQLException {
        List<Article> list;
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement statement = connection.prepareStatement(GET_LIMITED_PUBLISHED_ARTICLES_BY_TAG_ID_DESC_QUERY)) {

            statement.setLong(1, tagId);
            statement.setInt(2, (start-1));
            statement.setInt(3, total);
            ResultSet rs = statement.executeQuery();
            UserDAO userDAO = new UserDAOImpl();
            list = new ArrayList<>();
            TagDAO tagDAO = new TagDAOImpl();
            Set<Tag> tags;
            while (rs.next()) {
                tags = new HashSet<>();
                Long[] tagsId = null;
                if (rs.getArray("tags") != null) tagsId = (Long[]) rs.getArray("tags").getArray();

                if (tagsId != null) {
                    for (int i = 0; i < tagsId.length; i++) {
                        tags.add(tagDAO.getTag(tagsId[i]));
                    }
                }

                Article article = new Article();
                article.setId(rs.getLong("id"));
                article.setName(rs.getString("name"));
                article.setDescription(rs.getString("description"));
                article.setImageURL(rs.getString("image_url"));
                if (rs.getDate("publish_date") != null) article.setPublishDate(rs.getDate("publish_date").toLocalDate());
                if (rs.getDate("update_date") != null) article.setUpdateDate(rs.getDate("update_date").toLocalDate());
                article.setAuthor(userDAO.getUserById(rs.getLong("author_id")));
                article.setSummary(rs.getString("summary"));
                article.setTags(tags);

                list.add(article);
            }
        }

        return list;
    }

    @Override
    public long getNumberOfPublishedArticlesByTagId(Long id) throws SQLException {
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement statement = connection.prepareStatement(GET_PUBLISHED_ARTICLES_NUMBER_BY_TAG_ID);
             ) {

            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                return rs.getLong("recordsAmount");

            }
        }

        return 0;
    }
}
