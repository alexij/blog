package com.blog.dao.user;

import com.blog.model.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDAO {
    User insertUser(User user) throws SQLException;
    User getUserById(Long id) throws SQLException;
    User getUserByLogin(String login) throws SQLException;
    User getUserByEmail(String email) throws SQLException;
    User updateUser(User user) throws  SQLException;
    void deleteUser(Long id) throws SQLException;
    void switchUserStatusByEmail(String email, boolean status) throws SQLException;
    List<User> getLimitedUsers(int from, int to) throws SQLException;
    long getNumberOfUsers() throws SQLException;
    boolean userExists(String login) throws SQLException;
    boolean isActive(String login) throws SQLException;
    List<User> getAll() throws SQLException;
}
