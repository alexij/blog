package com.blog.dao.user;

import com.blog.model.Role;
import com.blog.model.User;
import com.blog.util.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAOImpl implements UserDAO {
    private static final String CREATE_USER_QUERY = "INSERT INTO users (name, login, password, role, email, is_active) VALUES (?, ?, ?, ?::roles, ?, ?)";
    private static final String GET_USER_BY_ID_QUERY = "SELECT * FROM users WHERE id = ?";
    private static final String GET_USER_BY_LOGIN_QUERY = "SELECT * FROM users WHERE login = ?";
    private static final String GET_USER_BY_EMAIL_QUERY = "SELECT * FROM users WHERE email = ?";
    private static final String UPDATE_USER_QUERY = "UPDATE users SET name = ?, password = ?, email = ?, role = ?, is_active = ? where id = ?";
    private static final String GET_ALL_USERS_QUERY = "SELECT * FROM users ORDER BY name ASC";
    private static final String GET_LIMITED_USERS_QUERY = "SELECT * FROM users ORDER BY name ASC OFFSET ? LIMIT ?";
    private static final String CHANGE_USER_ACCOUNT_STATUS_QUERY = "UPDATE users SET is_active = ? WHERE email = ?";
    private static final String GET_USERS_NUMBER_QUERY = "SELECT COUNT(id) as recordsAmount FROM users";
    private static final String USER_EXISTS_BY_LOGIN_QUERY = "SELECT id FROM users WHERE login = ?";
    private static final String IS_ACTIVE_BY_LOGIN_QUERY = "SELECT is_active FROM users WHERE login = ?";

    @Override
    public User insertUser(User user) throws SQLException {
        try(Connection connection = DBConnection.createConnection();
            PreparedStatement createStatement = connection.prepareStatement(CREATE_USER_QUERY);) {

            createStatement.setString(1, user.getName());
            createStatement.setString(2, user.getLogin());
            createStatement.setString(3, user.getPassword());
            createStatement.setString(4, user.getRole().name());
            createStatement.setString(5, user.getEmail());
            createStatement.setBoolean(6, true);
            createStatement.executeUpdate();

        }

        return user;
    }

    @Override
    public User getUserById(Long id) throws SQLException {
        User user = null;

        try (Connection connection = DBConnection.createConnection();
             PreparedStatement getUserById = connection.prepareStatement(GET_USER_BY_ID_QUERY);
             ) {
            getUserById.setLong(1, id);
            ResultSet rs = getUserById.executeQuery();

            if (rs.next()) {
                user = new User(rs.getString("name"),
                        rs.getString("login"),
                        rs.getString("password"),
                        rs.getString("email"),
                        Role.valueOf(rs.getString("role")),
                        rs.getBoolean("is_active"));
                user.setId(rs.getLong("id"));
            }
        }
        return user;
    }

    @Override
    public User getUserByLogin(String login) throws SQLException {
        User user = null;

        try (Connection connection = DBConnection.createConnection();
             PreparedStatement getUserByLogin = connection.prepareStatement(GET_USER_BY_LOGIN_QUERY);
        ) {
            getUserByLogin.setString(1, login);
            ResultSet rs = getUserByLogin.executeQuery();

            if (rs.next()) {
                user = new User(rs.getString("name"),
                        rs.getString("login"),
                        rs.getString("password"),
                        rs.getString("email"),
                        Role.valueOf(rs.getString("role")),
                        rs.getBoolean("is_active"));
                user.setId(rs.getLong("id"));
            }
        }
        return user;
    }

    @Override
    public User getUserByEmail(String email) throws SQLException {
        User user = null;

        try (Connection connection = DBConnection.createConnection();
             PreparedStatement getUserByEmail = connection.prepareStatement(GET_USER_BY_EMAIL_QUERY);
        ) {
            getUserByEmail.setString(1, email);
            ResultSet rs = getUserByEmail.executeQuery();

            if (rs.next()) {
                user = new User(rs.getString("name"),
                        rs.getString("login"),
                        rs.getString("password"),
                        rs.getString("email"),
                        Role.valueOf(rs.getString("role")),
                        rs.getBoolean("is_active"));
                user.setId(rs.getLong("id"));
            }
        }
        return user;
    }

    @Override
    public User updateUser(User user) throws SQLException {
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement updateUser = connection.prepareStatement(UPDATE_USER_QUERY);) {
            updateUser.setString(1, user.getName());
            updateUser.setString(2, user.getPassword());
            updateUser.setLong(3, user.getId());
            updateUser.setString(4, user.getEmail());
            updateUser.setString(5, user.getRole().name());
            updateUser.setBoolean(6, user.isActive());
            updateUser.executeUpdate();
        }
        return user;
    }

    //TODO think about what 'delete' actually has to do (maybe just DISABLE user's account?)
    @Override
    public void deleteUser(Long id) throws SQLException {

        String deleteUser = "DELETE FROM users WHERE id = ?";
        try(Connection connection = DBConnection.createConnection();
            PreparedStatement delete = connection.prepareStatement(deleteUser);) {

            delete.setLong(1, id);
            delete.executeUpdate();
        }
    }

    @Override
    public synchronized List<User> getAll() throws SQLException {
        List<User> list;
        try (Connection connection = DBConnection.createConnection();
             Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(GET_ALL_USERS_QUERY)) {

            list = new ArrayList<>();
            while (rs.next()) {
                list.add(new User(rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("login"),
                        rs.getString("password"),
                        rs.getString("email"),
                        Role.valueOf(rs.getString("role")),
                        rs.getBoolean("is_active")));
            }

        }

        return list;
    }

    @Override
    public void switchUserStatusByEmail(String email, boolean status) throws SQLException {
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement statement = connection.prepareStatement(CHANGE_USER_ACCOUNT_STATUS_QUERY)){
            statement.setBoolean(1, status);
            statement.setString(2, email);
            statement.executeUpdate();
        }
    }

    @Override
    public List<User> getLimitedUsers(int start, int total) throws SQLException {
        List<User> list = new ArrayList<>();
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement statement = connection.prepareStatement(GET_LIMITED_USERS_QUERY)) {

            statement.setInt(1, (start-1));
            statement.setInt(2, total);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                list.add(new User(rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("login"),
                        rs.getString("password"),
                        rs.getString("email"),
                        Role.valueOf(rs.getString("role")),
                        rs.getBoolean("is_active")));
            }
        }

        return list;
    }

    @Override
    public long getNumberOfUsers() throws SQLException {
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement getNumberOfUsers = connection.prepareStatement(GET_USERS_NUMBER_QUERY);
             ResultSet rs = getNumberOfUsers.executeQuery()) {

            if (rs.next()) {
                return rs.getLong("recordsAmount");

            }
        }

        return 0;
    }

    @Override
    public boolean userExists(String login) throws SQLException {
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement statement = connection.prepareStatement(USER_EXISTS_BY_LOGIN_QUERY);
        ){
            statement.setString(1, login);
            ResultSet rs = statement.executeQuery();
            return rs.next();
        }
    }

    @Override
    public boolean isActive(String login) throws SQLException {
        try (Connection connection = DBConnection.createConnection();
             PreparedStatement statement = connection.prepareStatement(IS_ACTIVE_BY_LOGIN_QUERY);
        ){
            statement.setString(1, login);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return rs.getBoolean("is_active");
            }
        }

        return false;
    }
}
