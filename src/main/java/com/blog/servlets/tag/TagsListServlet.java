package com.blog.servlets.tag;

import com.blog.dao.tag.TagDAOImpl;
import com.blog.model.Tag;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/tags")
public class TagsListServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(TagsListServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            List<Tag> tags = new TagDAOImpl().getAll();
            log.info("Retrieved a list of tags: " + tags);
            req.setAttribute("tagsList", tags);

        } catch (SQLException e) {
            log.info("Couldn't retrieve tags list.");
        }

        req.getRequestDispatcher("create-article.jsp").forward(req, resp);
    }
}
