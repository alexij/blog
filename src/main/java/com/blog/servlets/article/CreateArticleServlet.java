package com.blog.servlets.article;

import com.blog.model.Article;
import com.blog.model.Tag;
import com.blog.service.article.ArticleService;
import com.blog.service.article.ArticleServiceImpl;
import com.blog.service.tag.TagService;
import com.blog.service.tag.TagServiceImpl;
import com.blog.service.user.UserService;
import com.blog.service.user.UserServiceImpl;
import com.blog.util.UploadProperties;
import com.blog.util.validation.ModelValidator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@WebServlet("/createArticle")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
        maxFileSize=1024*1024*10,      // 10MB
        maxRequestSize=1024*1024*50)
public class CreateArticleServlet extends HttpServlet {
    private static final String SAVE_DIR = "uploaded";
    private static final Logger log = Logger.getLogger(CreateArticleServlet.class);

    private static final AtomicReference<TagService> tagService = new AtomicReference<>(new TagServiceImpl());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Tag> tags = null;

        try {
            tags = tagService.get().getAll();
            req.setAttribute("tagsList", tags);
            log.info("The list of tags: " + (tags.isEmpty() ? "it is empty." : tags));
        } catch (SQLException e) {
            log.info("Couldn't retrieve tags list.");
        }

        req.getRequestDispatcher("create-article.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("name");
        String description = req.getParameter("description");
        String summary = req.getParameter("summary");
        LocalDate publishDate = LocalDate.now();
        Part imageFilePart = req.getPart("file");

        String appPath = req.getServletContext().getRealPath("");
        String savePath = appPath + File.separator + SAVE_DIR;

        //String projectSavePath = UploadProperties.getUploadPath();


        File fileSaveDir = new File(savePath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdir();
        }

        /*File fileSaveProjectDir = new File(projectSavePath);
        if (!fileSaveProjectDir.exists()){
            fileSaveProjectDir.mkdir();
        }*/

        String fileNameUUID = UUID.randomUUID().toString();
        String fileName = fileNameUUID + '.' + Paths.get(imageFilePart.getSubmittedFileName()).getFileName().toString();
        fileName = new File(fileName).getName();
        if (imageFilePart.getSize() != 0) {
            imageFilePart.write(savePath + File.separator + fileName);
            //imageFilePart.write(projectSavePath + File.separator + fileName);
        }

        String[] selectedTagsStrings = req.getParameterValues("selectTags");
        Set<Tag> tags = new HashSet<>();
        if (selectedTagsStrings != null ) {
            for (int i = 0; i < selectedTagsStrings.length; i++) {
                try {
                    tags.add(tagService.get().getTag(Long.parseLong(selectedTagsStrings[i])));
                } catch (SQLException e) {
                    log.info("Couldn't retrieve tag with ID " + selectedTagsStrings[i]);
                }
            }
        }

        Article article = new Article();
        String userEmail = (String)req.getSession().getAttribute("userEmail");
        try{
            article.setAuthor(new AtomicReference<UserService>(new UserServiceImpl()).get().getUserByEmail(userEmail));
            try {
                article.setName(title);
                if (req.getParameter("draft") == null) {
                    article.setPublishDate(publishDate);
                    article.setUpdateDate(publishDate);
                }
                article.setDescription(description);

                article.setSummary(summary);
                log.info("Article summary: " + summary + " , length: " + summary.length());
                log.info("Article description: " + description + " , length: " + description.length());

                if (imageFilePart.getSize() != 0){
                    article.setImageURL(SAVE_DIR + File.separator + fileName);
                }else article.setImageURL("img\\ufo.jpg");

                article.setTags(tags);

                ModelValidator modelValidator = new ModelValidator();
                List<String> errorMessages = new ArrayList<>();
                errorMessages.addAll(modelValidator.validate(article));

                if (errorMessages.isEmpty()) {
                    log.info("Created article: " + new AtomicReference<ArticleService>(new ArticleServiceImpl()).get().insertArticle(article));
                }else{
                    log.info(errorMessages);
                    req.setAttribute("errorMessages", errorMessages);
                    req.getRequestDispatcher("create-article.jsp").forward(req, resp);
                }

            } catch (SQLException e) {
                log.info(e);
            }
        } catch (SQLException e) {
            log.info("Couldn't retrieve a user with email \"" + userEmail);
            log.info(e);
        }

        resp.sendRedirect(req.getContextPath() + "/articles?page=1");

    }
}
