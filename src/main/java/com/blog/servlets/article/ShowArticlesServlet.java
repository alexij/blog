package com.blog.servlets.article;

import com.blog.model.Article;
import com.blog.model.Tag;
import com.blog.service.article.ArticleService;
import com.blog.service.article.ArticleServiceImpl;
import com.blog.service.tag.TagService;
import com.blog.service.tag.TagServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

//TODO change the URL
@WebServlet("/articles")
public class ShowArticlesServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(ShowArticlesServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AtomicReference<ArticleService> articleService = new AtomicReference<>(new ArticleServiceImpl());
        AtomicReference<TagService> tagService = new AtomicReference<>(new TagServiceImpl());


        int currentPageId = Integer.parseInt(req.getParameter("page"));
        int totalPagesAmount = 5;

        try {
            long numberOfPages = articleService.get().getNumberOfPublishedArticles();
            req.setAttribute("totalPages", numberOfPages);
        } catch (SQLException e) {
            log.info("Couldn't retrieve the number of published articles.");
        }

        if (currentPageId != 1){
            currentPageId = currentPageId-1;
            currentPageId = currentPageId*totalPagesAmount+1;
        }

        try {
            List<Article> articles = articleService.get().getPublishedLimitedArticles(currentPageId, totalPagesAmount);
            req.setAttribute("articles", articles);

        } catch (SQLException e) {
            log.info(e);
        }

        List<Tag> tags = null;

        try {
            tags = tagService.get().getAll();
            req.setAttribute("tagsList", tags);
            log.info("The list of tags: " + (tags.isEmpty() ? "it is empty." : tags));
        } catch (SQLException e) {
            log.info(e);
        }

        req.getRequestDispatcher("home.jsp").forward(req, resp);
    }
}
