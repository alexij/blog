package com.blog.servlets.article;

import com.blog.model.Article;
import com.blog.model.Tag;
import com.blog.service.article.ArticleService;
import com.blog.service.article.ArticleServiceImpl;
import com.blog.service.tag.TagService;
import com.blog.service.tag.TagServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

@WebServlet("/editArticle")
public class EditArticleServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(EditArticleServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Article article = null;
        Long articleId = Long.parseLong(req.getParameter("article_id"));
        try{
            article = new AtomicReference<ArticleService>(new ArticleServiceImpl()).get().getArticle(articleId);
            log.info("Retrieved an article with ID " + articleId + ": " + article);
            req.setAttribute("article", article);

            List<Tag> tags = null;
            try{
                tags = new AtomicReference<TagService>(new TagServiceImpl()).get().getAll();
                log.info("Article tags: " + tags);
            }catch(SQLException e){
                log.info("Couldn't retrieve tags from database.");
            }

            req.setAttribute("tagsList", tags);

        } catch (SQLException e) {
            log.info("Couldn't retrieve article with ID " + articleId + ".");
        }

        req.getRequestDispatcher("edit-article.jsp").forward(req, resp);

    }
}
