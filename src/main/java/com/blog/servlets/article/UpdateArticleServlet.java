package com.blog.servlets.article;

import com.blog.model.Article;
import com.blog.model.Tag;
import com.blog.service.article.ArticleService;
import com.blog.service.article.ArticleServiceImpl;
import com.blog.service.tag.TagService;
import com.blog.service.tag.TagServiceImpl;
import com.blog.service.user.UserService;
import com.blog.service.user.UserServiceImpl;
import com.blog.util.UploadProperties;
import com.blog.util.validation.ModelValidator;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@WebServlet("/updateArticle")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
        maxFileSize=1024*1024*10,      // 10MB
        maxRequestSize=1024*1024*50)
public class UpdateArticleServlet extends HttpServlet {
    private static final String SAVE_DIR = "uploaded";
    private static final Logger log = Logger.getLogger(UpdateArticleServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("id"));
        String title = req.getParameter("name");
        String description = req.getParameter("description");
        String summary = req.getParameter("summary");
        String imageURL = req.getParameter("imageURL");
        Long authorId = Long.valueOf(req.getParameter("authorId"));
        LocalDate publishDate = null;
        LocalDate updateDate = LocalDate.now();

        Part imageFilePart = req.getPart("file");

        String fileName = "";
        if (imageFilePart.getSize() != 0) {
                String appPath = req.getServletContext().getRealPath("");
                String savePath = appPath + File.separator + SAVE_DIR;


                File fileSaveDir = new File(savePath);
                if (!fileSaveDir.exists()) {
                    fileSaveDir.mkdir();
                }

                String fileNameUUID = UUID.randomUUID().toString();
                fileName = fileNameUUID + '.' + Paths.get(imageFilePart.getSubmittedFileName()).getFileName().toString();
                fileName = new File(fileName).getName();
                imageFilePart.write(savePath + File.separator + fileName);
            }

        AtomicReference<TagService> tagService = new AtomicReference<>(new TagServiceImpl());
        String[] selectedTagsStrings = req.getParameterValues("selectTags");
        Set<Tag> tags = new HashSet<>();
        try{
            if (selectedTagsStrings != null) {
                for (int i = 0; i < selectedTagsStrings.length; i++) {
                    tags.add(tagService.get().getTag(Long.parseLong(selectedTagsStrings[i])));
                }
            }
                AtomicReference<ArticleService> articleService = new AtomicReference<>(new ArticleServiceImpl());
                Article article = new Article();

                article.setId(id);
                article.setName(title);
                if (req.getParameter("draft") == null) {
                    if (req.getParameter("publishDate").equals("")) publishDate = LocalDate.now();
                    else publishDate = LocalDate.parse(req.getParameter("publishDate"));
                    article.setPublishDate(publishDate);
                    article.setUpdateDate(updateDate);
                }else{
                    article.setPublishDate(null);
                    article.setUpdateDate(updateDate);
                }
                article.setDescription(description);
                article.setSummary(summary);
                article.setAuthor(new AtomicReference<UserService>(new UserServiceImpl()).get().getUser(authorId));

                if (imageFilePart.getSize() != 0) {
                    article.setImageURL(SAVE_DIR + File.separator + fileName);
                }else{
                    article.setImageURL(imageURL);
                }

                article.setTags(tags);

            ModelValidator modelValidator = new ModelValidator();
            List<String> errorMessages = new ArrayList<>();
            errorMessages.addAll(modelValidator.validate(article));

            if (errorMessages.isEmpty()){
                articleService.get().updateArticle(article);
            }
            else{
                req.setAttribute("errorMessages", errorMessages);
                log.info(errorMessages);
            }
            }catch(SQLException e){
                log.info(e);
            }



        resp.sendRedirect(req.getContextPath() + "/details?id=" + id);

    }
}
