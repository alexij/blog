package com.blog.servlets.article;

import com.blog.service.article.ArticleService;
import com.blog.service.article.ArticleServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

@WebServlet("/deleteArticle")
public class DeleteArticleServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(DeleteArticleServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long articleId = Long.valueOf(req.getParameter("article_id"));

        try {
            new AtomicReference<ArticleService>(new ArticleServiceImpl()).get().deleteArticle(articleId);
        } catch (SQLException e) {
            log.info("Couldn't delete an article with ID " + articleId + ".");
        }

        resp.sendRedirect(req.getContextPath() + "/articles?page=1");
    }
}
