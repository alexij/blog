package com.blog.servlets.article;

import com.blog.model.Article;
import com.blog.service.article.ArticleService;
import com.blog.service.article.ArticleServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

@WebServlet("/details")
public class ArticleDetailsServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(ArticleDetailsServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long articleId = Long.valueOf(req.getParameter("id"));
        Article article;

        try {
            article = new AtomicReference<ArticleService>(new ArticleServiceImpl()).get().getArticle(Long.parseLong(req.getParameter("id")));
            log.info("Retrieved article details: " + article);

            req.setAttribute("article", article);
            req.getRequestDispatcher("article-details.jsp").forward(req, resp);
        } catch (SQLException e) {
            log.info("Couldn't get the article with ID " + articleId + ".");
        }

    }
}
