package com.blog.servlets.user;

import com.blog.service.user.UserService;
import com.blog.service.user.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

@WebServlet("/switchUserStatus")
public class UserStatusServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(UserStatusServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userEmail = req.getParameter("email");
        boolean status = Boolean.parseBoolean(req.getParameter("value"));

        AtomicReference<UserService> userService = new AtomicReference<>(new UserServiceImpl());

        try {
            if (!userEmail.equals(req.getSession().getAttribute("userEmail")))
                userService.get().switchUserStatusByEmail(userEmail, status);
        } catch (SQLException e) {
            log.info(e);
        }

        resp.sendRedirect(req.getContextPath() + "/users?page=1");
    }
}
