package com.blog.servlets.user;

import com.blog.model.User;
import com.blog.service.user.UserService;
import com.blog.service.user.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@WebServlet("/users")
public class UsersListServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(UsersListServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AtomicReference<UserService> userService = new AtomicReference<>(new UserServiceImpl());

        int currentPageId = Integer.parseInt(req.getParameter("page"));
        int totalRecordsAmount = 10;

        try {
            long numberOfUsers = userService.get().getNumberOfUsers();
            req.setAttribute("totalUsers", numberOfUsers);
        } catch (SQLException e) {
            log.info(e);
        }

        if (currentPageId != 1){
            currentPageId = currentPageId-1;
            currentPageId = currentPageId*totalRecordsAmount+1;
        }

        try {
            List<User> users = userService.get().getLimitedUsers(currentPageId, totalRecordsAmount);
            req.setAttribute("users", users);
            log.info("Retrieved a list of users: " + users);

        } catch (SQLException e) {
            log.info(e);
        }

        req.getRequestDispatcher("users.jsp").forward(req, resp);
    }
}
