package com.blog.servlets.user;

import com.blog.model.User;
import com.blog.model.Role;
import com.blog.service.user.UserService;
import com.blog.service.user.UserServiceImpl;
import com.blog.util.validation.ModelValidator;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@WebServlet("/register")
public class UserRegisterServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(UserRegisterServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("username");
        String userLogin = req.getParameter("login");
        String userPassword = req.getParameter("password");
        String userPasswordConfirm = req.getParameter("confirmPassword");
        String userEmail = req.getParameter("email");

        ModelValidator modelValidator = new ModelValidator();
        List<String> errorMessages = new ArrayList<>();

        if (!userPassword.equals(userPasswordConfirm))
            errorMessages.add("Passwords don't match.");

        User user = new User(userName, userLogin, userPassword, userEmail, Role.User, true);
        errorMessages.addAll(modelValidator.validate(user));


        if (errorMessages.isEmpty()) {
            try {
                new AtomicReference<UserService>(new UserServiceImpl()).get().insertUser(user);
                log.info("Registered a user: " + user);
                req.setAttribute("registerMessage", "You have successfully registered an account! Now you can login.");
            } catch (SQLException e) {
                log.info("Couldn't create user: " + e.getMessage());
            } finally {
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("login.jsp");
                requestDispatcher.forward(req, resp);
            }
        }else{
            List<String> processedErrorMessages = new ArrayList<>();
            for (String s : errorMessages)
                processedErrorMessages.add(s.replaceAll("\n", "<br>"));
            req.setAttribute("message", processedErrorMessages);
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        }
    }
}
