package com.blog.servlets.user;

import com.blog.model.User;
import com.blog.service.user.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/login")
public class UserLoginServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(UserLoginServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userLogin = req.getParameter("login");
        String userPassword = req.getParameter("password");

        HttpSession session = req.getSession();
        User user;
        try {
            user = new UserServiceImpl().authenticateUser(userLogin, userPassword);
            if (user != null) {
                session.setAttribute("userEmail", user.getEmail());
                session.setAttribute("userName", user.getName());
                session.setAttribute("userRole", user.getRole());

                log.info("The authenticated user: " + user);

                session.setMaxInactiveInterval(10*60);
                resp.sendRedirect(req.getContextPath() + "/articles?page=1");
            }else{
                log.info("User with login " + "\'" + userLogin + "\'" + " has not been logged in - wrong credentials.");
                req.setAttribute("message", "Wrong credentials!");
                req.getRequestDispatcher("login.jsp").forward(req, resp);
            }
        } catch (SQLException e) {
            log.info("Couldn't retrieve user.");
            req.setAttribute("message", "Something went totally wrong.");
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        }

    }
}
