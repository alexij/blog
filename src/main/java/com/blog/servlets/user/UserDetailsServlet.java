package com.blog.servlets.user;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/userDetails")
public class UserDetailsServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(UserDetailsServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String email = req.getParameter("email");

        log.info("User details: " + "name - " +  name + " , " + email);

        req.setAttribute("userName", name);
        req.setAttribute("userEmail", email);

        req.getRequestDispatcher("user-article-details.jsp").forward(req, resp);
    }
}
