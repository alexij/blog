package com.blog.servlets.user;

import com.blog.model.Article;
import com.blog.model.User;
import com.blog.service.article.ArticleService;
import com.blog.service.article.ArticleServiceImpl;
import com.blog.service.user.UserService;
import com.blog.service.user.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@WebServlet("/userArticles")
public class UserArticlesServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(UserArticlesServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        User user = null;
        try {
            user = new AtomicReference<UserService>(new UserServiceImpl()).get().getUserByEmail(email);
        } catch (SQLException e) {
            log.info(e);
        }

        List<Article> articles = new ArrayList<>();
        try {
            articles = new AtomicReference<ArticleService>(new ArticleServiceImpl()).get().getArticlesByAuthorId(user.getId());
        } catch (SQLException e) {
            log.info(e);
        }

        req.setAttribute("articles", articles);
        req.getRequestDispatcher("user-articles.jsp").forward(req, resp);
    }
}
