package com.blog.util;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class UploadProperties {
    private static Properties properties = new Properties();
    private static final String PROPERTIES_FILE_NAME = "files.properties";
    private static final Logger log = Logger.getLogger(UploadProperties.class);

    private UploadProperties() {
        throw new IllegalStateException("Utility class");
    }

    public static String getUploadPath(){
        String uploadPathInProject = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try (InputStream inputStream = classLoader.getResourceAsStream(PROPERTIES_FILE_NAME)) {
            properties.load(inputStream);
            uploadPathInProject = properties.getProperty("uploadPath");
        } catch (IOException e) {
            log.info(e.getMessage());
        }
        return uploadPathInProject;
    }

}
