package com.blog.util;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private static final String URL = DBConnectionProperties.getUrl();
    private static final String LOGIN = DBConnectionProperties.getLogin();
    private static final String PASSWORD = DBConnectionProperties.getPassword();

    private static final Logger log = Logger.getLogger(DBConnection.class);

    private DBConnection() {
    }

    public static Connection createConnection() {
        Connection connection = null;

        try{
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            log.info("Couldn't find PostgreSQL driver.");
        }

        try{
            connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
        } catch (SQLException e) {
            log.info("Couldn't establish the connection with database (URL: " + URL + " , LOGIN: " + LOGIN + " , PASSWORD: " + PASSWORD + ").");
        }

        return connection;
    }
}