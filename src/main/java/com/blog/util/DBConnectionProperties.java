package com.blog.util;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnectionProperties {
    private static final String DATABASE_PROPERTIES_FILE_NAME = "database.properties";

    private static final Logger log = Logger.getLogger(DBConnectionProperties.class);
    private static Properties properties = new Properties();

    private DBConnectionProperties() {
        throw new IllegalStateException("Utility class.");
    }

    public static String getUrl(){
        String url = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try (InputStream inputStream = classLoader.getResourceAsStream(DATABASE_PROPERTIES_FILE_NAME)) {
            properties.load(inputStream);
            url = properties.getProperty("url");
        } catch (IOException e) {
            log.info(e.getMessage());
        }
        return url;

    }

    public static String getLogin(){
        String login = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try (InputStream inputStream = classLoader.getResourceAsStream(DATABASE_PROPERTIES_FILE_NAME)) {
            properties.load(inputStream);
            login = properties.getProperty("login");
        } catch (IOException e) {
            log.info(e.getMessage());
        }
        return login;
    }

    public static String getPassword(){
        String password = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try (InputStream inputStream = classLoader.getResourceAsStream(DATABASE_PROPERTIES_FILE_NAME)) {
            properties.load(inputStream);
            password = properties.getProperty("password");
        } catch (IOException e) {
            log.info(e.getMessage());
        }
        return password;
    }
}