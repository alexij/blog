<%--
  Created by IntelliJ IDEA.
  User: alexij
  Date: 09.07.2018
  Time: 23:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>

<html>
<head>
    <meta charset="utf-8">
    <title>Article edit</title>

</head>
<body>
<c:choose>
    <c:when test="${sessionScope.userRole != null}">
        <div id = "wrapper">
            <%@ include file="header.jsp"%>
            <div id="underHeaderLine"></div>
            <span style="color: RED">${requestScope.errorMessages}</span>
            <form action="${pageContext.request.contextPath}/updateArticle" method="post" enctype="multipart/form-data" id="editArticleForm">
                <input type="hidden" name="authorId" value="${article.author.id}">
                <input type="hidden" name="id" value="${article.id}">
                <input type="hidden" name="publishDate" value="${article.publishDate}">
                <input type="hidden" name="imageURL" value="${article.imageURL}">
                <label>Article's name</label><br>
                <input class="form-control" type="text" style="width: 500px" required maxlength="70" name="name" placeholder="Enter article's name (70 characters)..." value="${article.name}"><br>
                <label>Description</label><br>
                <textarea class="form-control"  required maxlength="240" name="description" placeholder="Add description (240 characters)...">${article.description}</textarea><br>
                <label>Summary</label><br>
                <textarea class="form-control"  name="summary" placeholder="Add summary...">${article.summary}</textarea><br>
                <img height="146" width="215" src="${article.imageURL}"/><br>
                <label>Image</label><br>
                <input class="form-group" type="file" name="file"/><br>
                <label>Current tags:
                    <c:forEach items="${article.tags}" var="tag">
                        <span class="label label-default">${tag.name}</span>
                    </c:forEach>
                </label><br>
                <label>Tags</label><br>
                <select name="selectTags" class="custom-select" multiple>
                    <c:forEach items="${tagsList}" var="tag">
                        <option value="${tag.id}" ${article.tags.contains(tag) ? 'selected' : ''}>${tag.name}</option>
                    </c:forEach>
                </select><br>
                <label>Draft
                    <input type="checkbox" class="form-check-input" name="draft" />
                </label><br>
                <button type="submit" class="btn btn-warning">Update</button>
            </form>
        </div>
    </c:when>
    <c:otherwise>
        You have no access to this page!
    </c:otherwise>
</c:choose>
</body>
</html>
