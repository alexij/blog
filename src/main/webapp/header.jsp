<%--
  Created by IntelliJ IDEA.
  User: alexij
  Date: 09.07.2018
  Time: 22:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script>
        <%@include file="js/jquery-3.3.1.min.js"%>
        <%@include file="js/bootstrap.js"%>
    </script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/styles.css">
</head>
<body>
<header style="background-image: url('${pageContext.request.contextPath}/img/header.jpg');">
    <nav>
        <ul>
            <li><a href="${pageContext.request.contextPath}/articles?page=1">Home</a></li>
            <c:if test="${sessionScope.userRole != null}">
                <li><a href="${pageContext.request.contextPath}/userArticles?email=${sessionScope.userEmail}">My articles</a></li>
            </c:if>
            <li><a href="#aboutModal" data-toggle="modal" data-target="#aboutModal">About</a></li>
            <li><a href="${pageContext.request.contextPath}/users?page=1">Users</a></li>
            <c:choose>
                <c:when test="${sessionScope.userRole != null}">
                    <li style="float: right"><a href="${pageContext.request.contextPath}/logout">Log out</a></li>
                </c:when>
                <c:otherwise>
                    <li style="float: right"><a href="${pageContext.request.contextPath}/login.jsp">Log in</a></li>
                </c:otherwise>
            </c:choose>
            <c:if test="${sessionScope.userRole != null}">
                <span id="welcome">
                    Welcome, <span style="color: #ff9d50"> ${sessionScope.userName}</span>!
                </span>
            </c:if>
        </ul>
    </nav>
</header>
    <div class="modal fade" id="aboutModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h2 class="modal-title text-center" >About</h2>
                </div>
                <div class="modal-body">
                    This is a blog developed by Olexij Buzilov for SoftServe IT Academy.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
