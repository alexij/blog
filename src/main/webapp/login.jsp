<%--
  Created by IntelliJ IDEA.
  User: obuzitc
  Date: 7/3/2018
  Time: 1:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>

<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/login-form.css">
    <script>
        <%@include file="/js/jquery-3.3.1.min.js"%>
        <%@include file="/js/bootstrap.js"%>
    </script>
</head>
<body class="bg">
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <a href="${pageContext.request.contextPath}/index.jsp"><button style="width: 100%; margin-bottom: 5px" type="button" class="btn btn-lg btn-default">Home</button></a>
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="#login-form" class="active" id="login-form-link">Login</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#register-form" id="register-form-link">Register</a>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="login-form" action="login" method="post" role="form" style="display: block;">
                                <div class="form-group">
                                    <input type="text" name="login" id="login" tabindex="1" class="form-control" placeholder="Login" value="">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" id="loginPassword" tabindex="2" class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="text-center">
                                                <span style="color: RED">${requestScope.message}</span><span style="color: GREEN">${requestScope.registerMessage}</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form id="register-form" action="register" method="post" role="form" style="display: none;">
                                <div class="form-group">
                                    <input type="text" name="login" id="registerLogin" pattern="^[A-Za-z]([A-Za-z0-9_]){5,29}$" tabindex="1" class="form-control" placeholder="Login" value="" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="username" id="registerUsername" pattern="^[A-Za-z]([A-Za-z0-9_]\s?){5,29}$" tabindex="1" class="form-control" placeholder="Username" value="" required>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" pattern="^([\w]\.?){6,30}@[\w]+(\.[\w]+)*(\.[a-z]{2,})$" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="" required>
                                </div>
                                <div class="form-group">
                                    <input hidden type="password" name="password" id="registerPassword" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=\S+$).{6,32}$" tabindex="2" class="form-control" placeholder="Password" required>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="confirmPassword" id="confirmPassword" tabindex="2" class="form-control" placeholder="Confirm Password" required>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div id="passwordMessage" class="message">
                    <h3>Password must match the following rules:</h3>
                    <p id="letter" class="invalid">Must contain a <b>lowercase</b> letter.</p>
                    <p id="capital" class="invalid">Must contain a <b>capital (uppercase)</b> letter.</p>
                    <p id="number" class="invalid">Must contain a <b>digit</b>.</p>
                    <p id="length" class="invalid">Must contain from <b>6 to 32 characters</b>.</p>
                    <p id="whitespace" class="valid">Must <b>not</b> contain whitespaces.</p>
                </div>
                <div id="emailMessage" class="message">
                    <h3>Email must match the following rules:</h3>
                    <p id="emailFirstPart" class="invalid">First part of email must contain from 6 to 30 alphanumeric characters.</p>
                    <p id="period" class="valid">Period is allowed (but not compulsory) once between other characters.</p>
                    <p id="atCharacter" class="invalid">Email must contain <b>one</b> '@' character.</p>
                    <p id="emailSecondPart" class="invalid">In the part after '@' character, there has to be at least 1 character and a dot after it. There has to be at least 2 characters after each period.</p>
                </div>
                <div id="nicknameMessage" class="message">
                    <h3>Nickname must match the following rules:</h3>
                    <p id="nicknameLength" class="invalid">Nickname must contain from 6 to 30 characters.</p>
                    <p id="nicknameCharacters" class="invalid">It must consist only of English alphanumeric characters, underscores and one whitespace
                        between each alphanumeric and underscore part.
                    </p>
                    <p id="nicknameBeginning" class="invalid">Nickname must begin with English alphabetic character.</p>
                </div>
                <div id="loginMessage" class="message">
                    <h3>Login must match the following rules:</h3>
                    <p id="loginLength" class="invalid">Login must contain from 6 to 30 characters.</p>
                    <p id="loginCharacters" class="invalid">It must consist only of English alphanumeric characters and underscores.
                    </p>
                    <p id="loginBeginning" class="invalid">Login must begin with English alphabetic character.</p>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
    <script>
        <%@include file="/js/login.js"%>
    </script>
</html>
