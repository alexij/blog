<%--
  Created by IntelliJ IDEA.
  User: alexij
  Date: 05.07.2018
  Time: 21:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>

<html>
<head>
    <meta charset="utf-8">
    <title>Article creation</title>
</head>

<body>
<c:choose>
<c:when test="${sessionScope.userRole != null}">
    <div id = "wrapper">
        <%@ include file="header.jsp"%>
        <div id="underHeaderLine"></div>
        <span style="color: RED">${requestScope.errorMessages}</span>
        <form action="${pageContext.request.contextPath}/createArticle" method="post" enctype="multipart/form-data" id="createArticleForm">
            <label>Article's name</label><br>
            <input class="form-control" type="text" style="width: 700px" required maxlength="70" name="name" placeholder="Enter article's name..."><br>
            <label>Description</label><br>
            <textarea class="form-control" required maxlength="240" name="description" placeholder="Add description (240 characters)..."></textarea><br>
            <label>Summary</label><br>
            <textarea class="form-control"  name="summary" placeholder="Add summary..."></textarea><br>
            <label>Image<br></label>
            <input class="form-group" type="file" name="file"/>
            <label>Tags</label><br>
            <select name="selectTags" class="custom-select" multiple>
                <c:forEach items="${tagsList}" var="tag">
                    <option value="${tag.id}">${tag.name}</option>
                </c:forEach>
            </select><br>
            <label>Draft
                <input class="form-check-input" type="checkbox" name="draft" />
            </label><br>
            <button type="submit" class="btn btn-primary">Create</button>
        </form>
    </div>
</c:when>
<c:otherwise>
    You have no access to this page!
</c:otherwise>
</c:choose>
</body>
</html>
