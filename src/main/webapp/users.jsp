<%--
  Created by IntelliJ IDEA.
  User: alexij
  Date: 13.07.2018
  Time: 13:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>

<html>
<head>
    <title>Users list</title>
</head>
<body>
    <div id = "wrapper">
        <%@ include file="header.jsp"%>
        <div id="underHeaderLine"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-11">
                        <div class="table-responsive table-body">
                            <table id="mytable" class="table table-bordered table-striped">
                                <thead>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th style="text-align: center">Active</th>
                                    <c:if test="${sessionScope.userRole == 'Admin'}">
                                        <th>Actions</th>
                                    </c:if>
                                </thead>
                                <tbody>
                                <c:forEach items="${users}" var="user" varStatus="vs">
                                    <tr>
                                        <td><a class="usersListUserNicknameAnchor" href="${pageContext.request.contextPath}/userArticles?email=${user.email}">${user.name}</a></td>
                                        <td>${user.email}</td>
                                        <td>${user.role}</td>
                                        <td style="text-align: center">${user.active ? '<span style="color: GREEN">✔</span>'
                                                            : '<span style="color: RED">✘</span>'}
                                        </td>
                                        <c:if test="${sessionScope.userRole == 'Admin'}">
                                            <td>
                                                <c:choose>
                                                    <c:when test="${user.active}">
                                                        <a href="${pageContext.request.contextPath}/switchUserStatus?email=${user.email}&value=false">Deactivate user</a>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <a href="${pageContext.request.contextPath}/switchUserStatus?email=${user.email}&value=true">Activate user</a>
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                        </c:if>
                                    </tr>
                                <div class="modal fade" id="userDetailsModal${vs.index}" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h2 class="modal-title text-center" id="userDetailsLabel">${user.name}</h2>
                                            </div>
                                            <div class="modal-body">
                                                <img src="img/user.png" name="aboutUser" width="140" height="140" border="0" class="img-circle center-block"><br>
                                                <h3 class="media-heading text-center"><a href="${pageContext.request.contextPath}/userArticles?email=${user.email}">View articles</a></h3>
                                                <hr>
                                                <p class="text-left"><strong>Email: </strong><br>
                                                        ${user.email}
                                                    <br>
                                                <p class="text-left"><strong>Role: </strong><br>
                                                        ${user.role}
                                                    <br>
                                                <p class="text-left"><strong>Status: </strong><br>
                                                        ${user.active ? '<span style="color: GREEN">Active&nbsp✔</span>'
                                                                : '<span style="color: RED;">Unactive&nbsp✘</span>'}
                                                    <br>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default right" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </c:forEach>

                                </table>
                            <ul class="pagination" id="usersPagination" >
                                <c:if test="${totalUsers != 1}">
                                    <c:forEach var="page" begin="1" end="${totalUsers}">
                                        <li class="page-item"><a href="${pageContext.request.contextPath}/users?page=${page}">${page}</a></li>
                                    </c:forEach>
                                </c:if>
                            </ul>
                </div>
            </div>
        </div>
    </div>

    </div>
</body>
</html>
