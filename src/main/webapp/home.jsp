<%--
  Created by IntelliJ IDEA.
  User: obuzitc
  Date: 6/29/2018
  Time: 12:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>

<html>
<head>
  <meta charset="utf-8">
  <title>Just some blog</title>
</head>
<body>
<div id = "wrapper">
  <%@ include file="header.jsp"%>
  <div id="underHeaderLine"></div>
  <div id="latestRecords">
      <div id="name">THE LATEST RECORDS</div>
      <c:if test="${sessionScope.userRole != null}">
          <a href="${pageContext.request.contextPath}/createArticle">Add new article</a>
      </c:if>
  </div>
    <div class="tagsList" align="center">
        Search by tags:
        <c:forEach items="${tagsList}" var="tag">
            <span class="label label-default"><a href="${pageContext.request.contextPath}/articlesByTag?page=1&id=${tag.id}">${tag.name}</a></span>
        </c:forEach>
    </div>
  <c:forEach items="${articles}" var="article" varStatus="vs">
          <div class="article">
              <div class="articleImage"><img height="146" width="215" src=<c:out value="${article.imageURL}"/> /> </div>
              <div id="articleHead">
                  <h1><c:out value="${article.name}"/></h1>
                  <div class="tags">
                      <c:forEach items="${article.tags}" var="tag">
                          <span class="label label-default">${tag.name}</span>
                      </c:forEach>
                  </div>
              </div>
              <p class="articleDescription"><c:out value="${article.description}"/> </p>
              <ul>
                  <li>Published on: ${article.publishDate}</li>
                  <li>Updated on: ${article.updateDate}</li>
                  <li>Author: <a href="#userDetailsModal${vs.index}" data-toggle="modal" data-target="#userDetailsModal${vs.index}">${article.author.name}</a></li>
              </ul>
              <a class="detailsBtn" href="${pageContext.request.contextPath}/details?id=${article.id}">Details</a>
              <c:if test="${article.author.email == sessionScope.userEmail}">
                  <form method="post" action="${pageContext.request.contextPath}/editArticle">
                      <input type="hidden" name="article_id" value="${article.id}">
                      <input type="submit" class="editBtn" value="">

                  </form>
                  <form method="post" action="${pageContext.request.contextPath}/deleteArticle" onsubmit="return confirm('Are you sure want to delete this article?');">
                      <input type="hidden" name="article_id" value="${article.id}">
                      <input type="submit" class="deleteBtn" value="">
                  </form>
              </c:if>
          </div>

      <div class="modal fade" id="userDetailsModal${vs.index}" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h2 class="modal-title text-center" id="userDetailsLabel">${article.author.name}</h2>
                  </div>
                  <div class="modal-body">
                          <img src="img/user.png" name="aboutUser" width="140" height="140" border="0" class="img-circle center-block"><br>
                          <h3 class="media-heading text-center"><a href="${pageContext.request.contextPath}/userArticles?email=${article.author.email}">View articles</a></h3>
                      <hr>
                          <p class="text-left"><strong>Email: </strong><br>
                              ${article.author.email}
                          <br>
                          <p class="text-left"><strong>Role: </strong><br>
                              ${article.author.role}
                          <br>
                          <p class="text-left"><strong>Status: </strong><br>
                              ${article.author.active ? '<span style="color: GREEN">Active&nbsp✔</span>'
                                      : '<span style="color: RED;">Unactive&nbsp✘</span>'}
                          <br>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default right" data-dismiss="modal">Close</button>
                  </div>
              </div>
          </div>
      </div>
  </c:forEach>

    <ul class="pagination" id="articlesPagination" >
        <c:if test="${totalPages != 1}">
            <c:forEach var="page" begin="1" end="${totalPages}" varStatus="vs">
                <li class="page-item"><a href="${pageContext.request.contextPath}/articles?page=${page}">${page}</a></li>
            </c:forEach>
        </c:if>
    </ul>
</div>

</body>

</html>
