$(function() {

    $('#login-form-link').click(function(e) {
        $("#login-form").delay(100).fadeIn(100);
        $("#register-form").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#register-form-link').click(function(e) {
        $("#register-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });

});

/* Password validation variables */
var password = document.getElementById("registerPassword");
var confirmPassword = document.getElementById("confirmPassword");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var length = document.getElementById("length");
var whitespace = document.getElementById("whitespace");

/* Email validation variables */
var email = document.getElementById("email");
var emailFirstPart = document.getElementById("emailFirstPart");
var emailSecondPart = document.getElementById("emailSecondPart");
var period = document.getElementById("period");
var atCharacter = document.getElementById("atCharacter");

/* Nickname validation variables */
var nickname = document.getElementById('registerUsername');
var nicknameLength = document.getElementById('nicknameLength');
var nicknameCharacters = document.getElementById('nicknameCharacters');
var nicknameBeginning = document.getElementById('nicknameBeginning');

/* Login validation variables */
var login = document.getElementById('registerLogin');
var loginLength = document.getElementById('loginLength');
var loginCharacters = document.getElementById('loginCharacters');
var loginBeginning = document.getElementById('loginBeginning');


// When the user clicks on the password field, show the message box
password.onfocus = function() {
    document.getElementById("passwordMessage").style.display = "block";
};

// When the user clicks outside of the password field, hide the message box
password.onblur = function() {
    document.getElementById("passwordMessage").style.display = "none";
};

// When the user clicks on the email field, show the message box
email.onfocus = function() {
    document.getElementById("emailMessage").style.display = "block";
};

// When the user clicks outside of the email field, hide the message box
email.onblur = function() {
    document.getElementById("emailMessage").style.display = "none";
};

// When the user clicks on the password field, show the message box
nickname.onfocus = function() {
    document.getElementById("nicknameMessage").style.display = "block";
};

// When the user clicks outside of the password field, hide the message box
nickname.onblur = function() {
    document.getElementById("nicknameMessage").style.display = "none";
};

// When the user clicks on the login field, show the message box
login.onfocus = function() {
    document.getElementById("loginMessage").style.display = "block";
};

// When the user clicks outside of the login field, hide the message box
login.onblur = function() {
    document.getElementById("loginMessage").style.display = "none";
};


// When the user starts to type something inside the password field
password.onkeyup = function() {
    // Validate lowercase letters
    var lowerCaseLetters = /[a-z]/g;
    if(password.value.match(lowerCaseLetters)) {
        letter.classList.remove("invalid");
        letter.classList.add("valid");
    } else {
        letter.classList.remove("valid");
        letter.classList.add("invalid");
    }

    // Validate capital letters
    var upperCaseLetters = /[A-Z]/g;
    if(password.value.match(upperCaseLetters)) {
        capital.classList.remove("invalid");
        capital.classList.add("valid");
    } else {
        capital.classList.remove("valid");
        capital.classList.add("invalid");
    }

    // Validate numbers
    var numbers = /[0-9]/g;
    if(password.value.match(numbers)) {
        number.classList.remove("invalid");
        number.classList.add("valid");
    } else {
        number.classList.remove("valid");
        number.classList.add("invalid");
    }

    // Validate length
    if(password.value.length >= 6) {
        length.classList.remove("invalid");
        length.classList.add("valid");
    } else {
        length.classList.remove("valid");
        length.classList.add("invalid");
    }

    // Check if there are no whitespaces
    if (password.value.indexOf(' ') < 0){
        whitespace.classList.remove("invalid");
        whitespace.classList.add("valid");
    }else{
        whitespace.classList.remove("valid");
        whitespace.classList.add("invalid");
    }

};

confirmPassword.onchange = function(){
    if (confirmPassword.value === password.value){
        confirmPassword.setCustomValidity("");
    }else{
        confirmPassword.setCustomValidity("Passwords don't match.");
    }
};

password.onchange = function (){
    var passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=\S+$).{6,32}$/;
    if (password.value.match(passwordPattern)){
        password.setCustomValidity("");
    }else{
        password.setCustomValidity("Password doesn't follow the rules!");
    }
};

email.onchange = function (){
    var emailPattern = /^([\w]\.?){6,30}@[\w]+(\.[\w]+)*(\.[a-z]{2,})$/;
    if (email.value.match(emailPattern)){
        email.setCustomValidity("");
    }else{
        email.setCustomValidity("Email doesn't follow the rules!");
    }

};

nickname.onchange = function (){
    var nicknamePattern = /^[A-Za-z]([A-Za-z0-9_]\s?){5,29}$/;

    if (nickname.value.match(nicknamePattern)){
        nickname.setCustomValidity("");
    }else{
        nickname.setCustomValidity("Nickname doesn't follow the rules!");
    }
};

login.onchange = function (){
    var loginPattern = /^[A-Za-z]([A-Za-z0-9_]){5,29}$/;

    if (login.value.match(loginPattern)){
        login.setCustomValidity("");
    }else{
        login.setCustomValidity("Login doesn't follow the rules!");
    }
};

/* Email validation */
function validAtCharacterNumber(string){
    var count = 0;
    for (var i = 0; i < string.length; i++){
        if (string.charAt(i) == '@')
            count++;

        if (count > 1) return false;
    }

    return count == 1;
}


// When the user starts to type something inside the email field
email.onkeyup = function(){
    // Checking the number of '@' character.
    if (validAtCharacterNumber(email.value)){
        atCharacter.classList.remove("invalid");
        atCharacter.classList.add("valid");
    }else{
        atCharacter.classList.remove("valid");
        atCharacter.classList.add("invalid");
    }

    // Checking if there are no periods IN A ROW.
    var moreThanOnePeriodPattern = /\w*\.{2,}\w*/;
    if (email.value.match(moreThanOnePeriodPattern)){
        period.classList.remove("valid");
        period.classList.add("invalid");
    }else{
        period.classList.remove("invalid");
        period.classList.add("valid");
    }

    var actualEmail = email.value.split("@");
    var firstPartPattern = /^([\w]\.?){6,30}$/;

    if (actualEmail.length !== 0 && actualEmail[0].match(firstPartPattern)){
        emailFirstPart.classList.remove("invalid");
        emailFirstPart.classList.add("valid");
    }else{
        emailFirstPart.classList.remove("valid");
        emailFirstPart.classList.add("invalid");
    }

    var secondPartPattern = /^[\w]+(\.[\w]+)*(\.[a-z]{2,})$/;
    if (validAtCharacterNumber(email.value) && actualEmail[1].match(secondPartPattern)){
        emailSecondPart.classList.remove("invalid");
        emailSecondPart.classList.add("valid");
    }else{
        emailSecondPart.classList.remove("valid");
        emailSecondPart.classList.add("invalid");
    }
};

/* Nickname validation */
nickname.onkeyup = function(){
    // Length validation
    if (nickname.value.length >= 6 && nickname.value.length <= 30){
        nicknameLength.classList.remove("invalid");
        nicknameLength.classList.add("valid");
    }else{
        nicknameLength.classList.remove("valid");
        nicknameLength.classList.add("invalid");
    }

    // Beginning validation
    if (nickname.value.charAt(0).match(/[A-Za-z]/)){
        nicknameBeginning.classList.remove("invalid");
        nicknameBeginning.classList.add("valid");
    }else{
        nicknameBeginning.classList.remove("valid");
        nicknameBeginning.classList.add("invalid");
    }

    var charactersPattern = /^([A-Za-z0-9_]\s?)*$/;
    if (nickname.value !== "" && nickname.value.match(charactersPattern)){
        nicknameCharacters.classList.remove("invalid");
        nicknameCharacters.classList.add("valid");
    }else{
        nicknameCharacters.classList.remove("valid");
        nicknameCharacters.classList.add("invalid");
    }

};

/* Login validation */
login.onkeyup = function(){
    // Length validation
    if (login.value.length >= 6 && login.value.length <= 30){
        loginLength.classList.remove("invalid");
        loginLength.classList.add("valid");
    }else{
        loginLength.classList.remove("valid");
        loginLength.classList.add("invalid");
    }

    // Beginning validation
    if (login.value.charAt(0).match(/[A-Za-z]/)){
        loginBeginning.classList.remove("invalid");
        loginBeginning.classList.add("valid");
    }else{
        loginBeginning.classList.remove("valid");
        loginBeginning.classList.add("invalid");
    }

    var charactersPattern = /^([A-Za-z0-9_])*$/;
    if (login.value !== "" && login.value.match(charactersPattern)){
        loginCharacters.classList.remove("invalid");
        loginCharacters.classList.add("valid");
    }else{
        loginCharacters.classList.remove("valid");
        loginCharacters.classList.add("invalid");
    }

};