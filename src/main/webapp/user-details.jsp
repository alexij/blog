<%--
  Created by IntelliJ IDEA.
  User: IIpOc
  Date: 11.07.2018
  Time: 14:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>

<html>
<head>
    <title>${requestScope.userName}'s profile</title>
</head>
<body>
<div id = "wrapper">
    <%@ include file="header.jsp"%>
    <div id="underHeaderLine"></div>
    <span>${requestScope.userName}</span>
    <span>${requestScope.userEmail}</span>
    <a href="${pageContext.request.contextPath}/userArticles?email=${requestScope.userEmail}">View articles</a>
</div>
</body>
</html>
