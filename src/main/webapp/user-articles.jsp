<%--
  Created by IntelliJ IDEA.
  User: obuzitc
  Date: 6/29/2018
  Time: 12:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>

<html>
<head>
  <meta charset="utf-8">
  <title>${sessionScope.userName}'s articles</title>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md12">
<div id = "wrapper">
  <%@ include file="header.jsp"%>
  <div id="underHeaderLine"></div>
    <c:choose>
        <c:when test="${articles.isEmpty()}">
            This user has no drafts/published articles.
        </c:when>
        <c:otherwise>
            <div class="userArticle">
                <c:forEach items="${articles}" var="article">
                    <c:choose>
                        <c:when test="${article.author.email == sessionScope.userEmail && article.publishDate == null}">
                            <h2><a href="${pageContext.request.contextPath}/details?id=${article.id}">${article.name}</a>
                                <c:if test="${article.publishDate == null}">
                                    <span style="color: RED">Draft</span>
                                </c:if>
                            </h2>
                        </c:when>
                        <c:when test="${article.publishDate != null}">
                            <h2><a href="${pageContext.request.contextPath}/details?id=${article.id}">${article.name}</a></h2>
                        </c:when>
                    </c:choose>
                    <c:if test="${article.author.email == sessionScope.userEmail}">
                        <form class="articleActions" method="post" action="${pageContext.request.contextPath}/editArticle">
                            <input type="hidden" name="article_id" value="${article.id}">
                            <input type="submit" class="editBtn" value="">
                        </form>
                        <form class="articleActions" method="post" action="${pageContext.request.contextPath}/deleteArticle" onsubmit="return confirm('Are you sure want to delete this article?');">
                            <input type="hidden" name="article_id" value="${article.id}">
                            <input type="submit" class="deleteBtn" value="">
                        </form>
                    </c:if>
                    <div class="clear"></div>
                </c:forEach>
            </div>
        </c:otherwise>
    </c:choose>
</div>
</div>
  <div id="loginModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-summary">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form name="loginForm" action="${pageContext.request.contextPath}/login" method="post">
              <input type="text" name="login" style="width: 100%"  placeholder="Login"/><br>
              <input type="password" name="password" style="width: 100%" placeholder="Password"/><br><br>
              <input type="submit" value="Log in" class="btn btn-success" style="width: 100%"/><br>
              <div align="center">Don't have an account yet? <a data-toggle="modal" data-target="#registerModal" data-dismiss="modal">Register</a></div>
          </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

    <div id="registerModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-summary">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form name="loginForm" action="${pageContext.request.contextPath}/register" method="post">
                        <input type="text" minlength="1" name="username" placeholder="Username"/><br>
                        <input type="text" minlength="1" maxlength="32" name="login" placeholder="Login"/><br>
                        <input id="password" name="password" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Must have at least 6 characters' : ''); if(this.checkValidity()) form.confirmPassword.pattern = this.value;" placeholder="Password" required><br>
                        <input id="confirmPassword" name="confirmPassword" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same password as above' : '');" placeholder="Verify password" required><br>
                        <input class="btn btn-success" type="submit" value="Register" width="200"/>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
  
</div>
</div>
</body>

</html>
