<%--
  Created by IntelliJ IDEA.
  User: alexij
  Date: 05.07.2018
  Time: 20:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>

<html>
<head>
    <meta charset="utf-8">
    <title>Блог</title>
</head>

<body>
    <div id = "wrapper">
        <%@ include file="header.jsp"%>
        <div id="underHeaderLine"></div>
        <div id="details">
            <h1 align="center">${requestScope.article.name}</h1>
            <img id="detailsArticleImage" src="${requestScope.article.imageURL}" />
            <div id="detailsSummary">${requestScope.article.summary}</div>
            <ul>
                <li>Published on: ${requestScope.article.publishDate}</li>
                <li>Updated on: ${requestScope.article.updateDate}</li>
                <li>Author: <a href="#userDetailsModal" data-toggle="modal" data-target="#userDetailsModal">${requestScope.article.author.name}</a></li>
            </ul>
            <div class="modal fade" id="userDetailsModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h2 class="modal-title text-center" id="userDetailsLabel">${requestScope.article.author.name}</h2>
                        </div>
                        <div class="modal-body">
                            <img src="img/user.png" name="aboutUser" width="140" height="140" border="0" class="img-circle center-block"><br>
                            <h3 class="media-heading text-center"><a href="${pageContext.request.contextPath}/userArticles?email=${requestScope.article.author.email}">View articles</a></h3>
                            <hr>
                            <p class="text-left"><strong>Email: </strong><br>
                                ${requestScope.article.author.email}
                                <br>
                            <p class="text-left"><strong>Role: </strong><br>
                                ${requestScope.article.author.role}
                                <br>
                            <p class="text-left"><strong>Status: </strong><br>
                                ${requestScope.article.author.active ? '<span style="color: GREEN">Active&nbsp✔</span>'
                                        : '<span style="color: RED;">Unactive&nbsp✘</span>'}
                                <br>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default right" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</body>
</html>
