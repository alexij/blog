PGDMP                          v           buz_blog    9.6.1    9.6.1     `           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            a           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            b           0    0 
   SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3            7           1247    388028    roles    TYPE     7   CREATE TYPE roles AS ENUM (
    'Admin',
    'User'
);
    DROP TYPE public.roles;
       public       postgres    false    3            �            1259    396239    articles    TABLE     %  CREATE TABLE articles (
    id bigint NOT NULL,
    name character varying(70) NOT NULL,
    description character varying(300) NOT NULL,
    image_source character varying NOT NULL,
    creation_date date,
    update_date date,
    author_id bigint NOT NULL,
    summary character varying
);
    DROP TABLE public.articles;
       public         postgres    false    3            �            1259    396237    articles_id_seq    SEQUENCE     q   CREATE SEQUENCE articles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.articles_id_seq;
       public       postgres    false    189    3            c           0    0    articles_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE articles_id_seq OWNED BY articles.id;
            public       postgres    false    188            �            1259    387969 
   auto_id_users    SEQUENCE     o   CREATE SEQUENCE auto_id_users
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.auto_id_users;
       public       postgres    false    3            �            1259    387993    users    TABLE     �   CREATE TABLE users (
    id bigint NOT NULL,
    name character varying NOT NULL,
    login character varying NOT NULL,
    password character varying NOT NULL,
    role roles NOT NULL
);
    DROP TABLE public.users;
       public         postgres    false    3    567            �            1259    387991    users_id_seq    SEQUENCE     n   CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    3    187            d           0    0    users_id_seq    SEQUENCE OWNED BY     /   ALTER SEQUENCE users_id_seq OWNED BY users.id;
            public       postgres    false    186            �           2604    396242    articles id    DEFAULT     \   ALTER TABLE ONLY articles ALTER COLUMN id SET DEFAULT nextval('articles_id_seq'::regclass);
 :   ALTER TABLE public.articles ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    188    189    189            �           2604    387996    users id    DEFAULT     V   ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    187    186    187            ]          0    396239    articles 
   TABLE DATA               p   COPY articles (id, name, description, image_source, creation_date, update_date, author_id, summary) FROM stdin;
    public       postgres    false    189   O       e           0    0    articles_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('articles_id_seq', 8, true);
            public       postgres    false    188            f           0    0 
   auto_id_users    SEQUENCE SET     4   SELECT pg_catalog.setval('auto_id_users', 3, true);
            public       postgres    false    185            [          0    387993    users 
   TABLE DATA               9   COPY users (id, name, login, password, role) FROM stdin;
    public       postgres    false    187   �       g           0    0    users_id_seq    SEQUENCE SET     3   SELECT pg_catalog.setval('users_id_seq', 7, true);
            public       postgres    false    186            �           2606    388001    users users_pkey 
   CONSTRAINT     G   ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    187    187            �           1259    396251    fki_author_fkey    INDEX     ;   CREATE INDEX fki_author_fkey ON articles USING btree (id);
 #   DROP INDEX public.fki_author_fkey;
       public         postgres    false    189            �           2606    396252    articles author_fkey 
   FK CONSTRAINT     g   ALTER TABLE ONLY articles
    ADD CONSTRAINT author_fkey FOREIGN KEY (author_id) REFERENCES users(id);
 >   ALTER TABLE ONLY public.articles DROP CONSTRAINT author_fkey;
       public       postgres    false    2017    189    187            ]   �   x���tJMUH������T�̼�|=�crf���g�T��$��'�d&��e�sZ���� 3�9�S��8!*J��WL�^��K<^)�OL�=�����+�MJ-R(�(Jr������ 4��      [   [   x�3���I����L�P*F�*�*�!�y�aF%A�^���nnN��E��)�%F�iY.%�!y���Qa�~���)��y\1z\\\ �6�     