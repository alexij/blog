PGDMP         +    
            v           postgres    10.4    10.4                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    4            P           1247    16432    roles    TYPE     >   CREATE TYPE public.roles AS ENUM (
    'Admin',
    'User'
);
    DROP TYPE public.roles;
       public       postgres    false    4            �            1259    16437    articles    TABLE     ;  CREATE TABLE public.articles (
    id bigint NOT NULL,
    name character varying(70) NOT NULL,
    description character varying(240) NOT NULL,
    image_url character varying,
    update_date date,
    author_id bigint NOT NULL,
    summary character varying,
    tags bigint[] NOT NULL,
    publish_date date
);
    DROP TABLE public.articles;
       public         postgres    false    4            �            1259    16443    articles_id_seq    SEQUENCE     x   CREATE SEQUENCE public.articles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.articles_id_seq;
       public       postgres    false    4    198                       0    0    articles_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.articles_id_seq OWNED BY public.articles.id;
            public       postgres    false    199            �            1259    16445    auto_id_users    SEQUENCE     v   CREATE SEQUENCE public.auto_id_users
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.auto_id_users;
       public       postgres    false    4            �            1259    16481    tags    TABLE     Q   CREATE TABLE public.tags (
    id bigint NOT NULL,
    name character varying
);
    DROP TABLE public.tags;
       public         postgres    false    4            �            1259    16479    tags_id_seq    SEQUENCE     t   CREATE SEQUENCE public.tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.tags_id_seq;
       public       postgres    false    4    204                       0    0    tags_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.tags_id_seq OWNED BY public.tags.id;
            public       postgres    false    203            �            1259    16447    users    TABLE       CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(32) NOT NULL,
    login character varying(32) NOT NULL,
    password character varying NOT NULL,
    role public.roles NOT NULL,
    email character varying,
    is_active boolean
);
    DROP TABLE public.users;
       public         postgres    false    592    4            �            1259    16453    users_id_seq    SEQUENCE     u   CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    4    201                       0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    202            �
           2604    16455    articles id    DEFAULT     j   ALTER TABLE ONLY public.articles ALTER COLUMN id SET DEFAULT nextval('public.articles_id_seq'::regclass);
 :   ALTER TABLE public.articles ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    199    198            �
           2604    16484    tags id    DEFAULT     b   ALTER TABLE ONLY public.tags ALTER COLUMN id SET DEFAULT nextval('public.tags_id_seq'::regclass);
 6   ALTER TABLE public.tags ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    204    203    204            �
           2604    16456    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    202    201                      0    16437    articles 
   TABLE DATA               y   COPY public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) FROM stdin;
    public       postgres    false    198   �                 0    16481    tags 
   TABLE DATA               (   COPY public.tags (id, name) FROM stdin;
    public       postgres    false    204   �                 0    16447    users 
   TABLE DATA               R   COPY public.users (id, name, login, password, role, email, is_active) FROM stdin;
    public       postgres    false    201   B                   0    0    articles_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.articles_id_seq', 36, true);
            public       postgres    false    199                       0    0    auto_id_users    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.auto_id_users', 3, true);
            public       postgres    false    200                       0    0    tags_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.tags_id_seq', 6, true);
            public       postgres    false    203                       0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 29, true);
            public       postgres    false    202            �
           2606    16489    tags tags_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.tags DROP CONSTRAINT tags_pkey;
       public         postgres    false    204            �
           2606    16519     users users_name_login_email_key 
   CONSTRAINT     i   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_name_login_email_key UNIQUE (name, login, email);
 J   ALTER TABLE ONLY public.users DROP CONSTRAINT users_name_login_email_key;
       public         postgres    false    201    201    201            �
           2606    16458    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    201            �
           1259    16459    fki_author_fkey    INDEX     B   CREATE INDEX fki_author_fkey ON public.articles USING btree (id);
 #   DROP INDEX public.fki_author_fkey;
       public         postgres    false    198            �
           2606    16460    articles author_fkey    FK CONSTRAINT     u   ALTER TABLE ONLY public.articles
    ADD CONSTRAINT author_fkey FOREIGN KEY (author_id) REFERENCES public.users(id);
 >   ALTER TABLE ONLY public.articles DROP CONSTRAINT author_fkey;
       public       postgres    false    2699    198    201                 x���n� ���S� Ղ���f�=�Ћt���Fؓ��~٤MoM��af�A
�c�1�v�uNO�f��8��XY0>�>d�~<����f�!F}�1C0Cg|��"D�6~i�:�Z���v��h��Lc�x��s�R�KE	���a�7��G�'�Ҥq�U� ����r�����V
(&����Yl��rr�T������'8e"sp��?�s>=�6�Z�T�M%Ң�(U��i)ۢP�R-�Ll!��=�u�h�`��O����~��K�ڼrsM�$���Ug         B   x�3�)J,K����K�2���2��HM�)��2�.�/*)�2��J,K�2�tO�K-J������ �[&           x�]�I��0F��W���������:�	�I&�_��]�9��"9����=E�(�|����?������'�5Q�~ �.V����k���3�X�s��G���(�d0J1��@3Y���1�Yo̏%�P�Lfz?�,✣�y�����s��ɽ���\�t+,3`֨��	t֏B��ˀk��&�b���b�]���U��`﷦y�f�C;�;���A���ƾ�O�"}̑�i�)���^�v��D.�4G4?�vڲ����.5B4�s�����_���)��
6h\��nԔ���"�2���̯����+x٥����I�G��hPV.	�>5Oֻ�~��QR�/��Fa�G�V;w_�#���+�6�2�h�nÕ�"��rHͫ���In�w��Q��/��yс$�z��)	g%
NpB�D�ay�oxFo�f��*H3�Y7�$ݒ�,�j�m���A�n�w%��E��^n��u�m7gŻ
�mO��3q�@�Bi�_��68��R5�a����k�ȝ��1�]��g�%^Q���~(m��{<�ʉ��c�XN���m��ޥWS�Rm���/�d�(k����ğ�8p� �n���åZ{�0��Q�
�Dvȳ+���X�R.݊�XIPY����z�o��i�B� Cy�:j���[�-9å[S=�g9���`j��@K�{�`��ٓ�`��659�ڦxwP��&�/��;z�N�h�T��Α/� �l�!��K+�}V����V�:@i�;23%�nK��M��L&�:�z     