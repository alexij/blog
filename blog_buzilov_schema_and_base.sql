--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: buz_blog_database; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE buz_blog_database WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Ukrainian_Ukraine.1251' LC_CTYPE = 'Ukrainian_Ukraine.1251';


ALTER DATABASE buz_blog_database OWNER TO postgres;

\connect buz_blog_database

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: roles; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.roles AS ENUM (
    'Admin',
    'User'
);


ALTER TYPE public.roles OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: articles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.articles (
    id bigint NOT NULL,
    name character varying(70) NOT NULL,
    description character varying(240) NOT NULL,
    image_url character varying,
    update_date date,
    author_id bigint NOT NULL,
    summary character varying,
    tags bigint[],
    publish_date date
);


ALTER TABLE public.articles OWNER TO postgres;

--
-- Name: articles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.articles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_id_seq OWNER TO postgres;

--
-- Name: articles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.articles_id_seq OWNED BY public.articles.id;


--
-- Name: auto_id_users; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auto_id_users
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auto_id_users OWNER TO postgres;

--
-- Name: tags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tags (
    id bigint NOT NULL,
    name character varying
);


ALTER TABLE public.tags OWNER TO postgres;

--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tags_id_seq OWNER TO postgres;

--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tags_id_seq OWNED BY public.tags.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(32) NOT NULL,
    login character varying(32) NOT NULL,
    password character varying NOT NULL,
    role public.roles NOT NULL,
    email character varying,
    is_active boolean
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: articles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.articles ALTER COLUMN id SET DEFAULT nextval('public.articles_id_seq'::regclass);


--
-- Name: tags id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tags ALTER COLUMN id SET DEFAULT nextval('public.tags_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: articles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) VALUES (37, 'asdsadasd', 'asdasdasd', 'img\ufo.jpg', '2018-07-17', 32, 'asdasd', '{4}', '2018-07-17');
INSERT INTO public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) VALUES (45, 'a', 'dasdasdddsdsdsdsdssssssssssssssssssssssssssssssssssssssssasdasdsdasdasdasdasdasdasdasdasdasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasdasdasdasdaasdasdasd', 'img\ufo.jpg', '2018-07-18', 10, '', '{}', '2018-07-18');
INSERT INTO public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) VALUES (19, 'fafadfadf', 'dasdasdddsdsdsdsdssssssssssssssssssssssssssssssssssssssssasdasdsdasdasdasdasdasdasdasdasdasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasd', 'img\ufo.jpg', '2018-07-10', 12, 'fasdgsdghdsfggsdfgsdfg', '{2}', '2018-07-10');
INSERT INTO public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) VALUES (18, 'fdsfa', 'dasdasdddsdsdsdsdssssssssssssssssssssssssssssssssssssssssasdasdsdasdasdasdasdasdasdasdasdasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasd', 'img\ufo.jpg', '2018-07-10', 12, 'aaaaa', '{1}', '2018-07-10');
INSERT INTO public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) VALUES (35, 'dasd', 'dasdasdddsdsdsdsdssssssssssssssssssssssssssssssssssssssssasdasdsdasdasdasdasdasdasdasdasdasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasd', 'img\ufo.jpg', '2018-07-15', 10, 'asdasdasdasd', '{4}', '2018-07-15');
INSERT INTO public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) VALUES (46, 'asdasd', 'dasd', 'img\ufo.jpg', '2018-07-18', 10, '<b>Strong text>', '{}', '2018-07-18');
INSERT INTO public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) VALUES (47, 'Health', 'Health description', 'uploaded\77eceada-6d21-41aa-90c0-eff0c068163f.coin.png', '2018-07-18', 10, 'Health summary', '{3}', '2018-07-18');
INSERT INTO public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) VALUES (31, 'I need 70 characters in order to test it. Something. Words. Sentences.', 'dasdasdddsdsdsdsdssssssssssssssssssssssssssssssssssssssssasdasdsdasdasdasdasdasdasdasdasdasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasddasdasd', 'img\ufo.jpg', '2018-07-17', 10, 'asdasdasd', '{3,2}', '2018-07-17');
INSERT INTO public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) VALUES (36, 'gdf', 'gdfgdfg', 'img\ufo.jpg', '2018-07-15', 10, 'dfgdfgdfg', '{4}', '2018-07-15');
INSERT INTO public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) VALUES (38, 'asdasd', 'asdasdasd', 'img\ufo.jpg', '2018-07-17', 10, 'adasdasda', NULL, '2018-07-17');
INSERT INTO public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) VALUES (43, 'afff', 'a', 'img\ufo.jpg', '2018-07-18', 10, '', '{1}', '2018-07-18');
INSERT INTO public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) VALUES (42, 'aaaaa', 'asdasd', 'img\ufo.jpg', '2018-07-18', 10, '', '{7}', '2018-07-18');
INSERT INTO public.articles (id, name, description, image_url, update_date, author_id, summary, tags, publish_date) VALUES (39, 'gdg', 'fdg', 'img\ufo.jpg', '2018-07-18', 10, '', '{}', NULL);


--
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.tags (id, name) VALUES (1, 'Travelling');
INSERT INTO public.tags (id, name) VALUES (2, 'IT');
INSERT INTO public.tags (id, name) VALUES (3, 'Health');
INSERT INTO public.tags (id, name) VALUES (4, 'Sports');
INSERT INTO public.tags (id, name) VALUES (5, 'Java');
INSERT INTO public.tags (id, name) VALUES (6, 'General');
INSERT INTO public.tags (id, name) VALUES (7, 'NBA');


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (12, 'vaas', 'vaas123', '$2a$10$0NP8yyUedjE23NozFc.2VeVoXvzXSQUUYxmnb4btc5jAhP3pYQNBm', 'User', 'a1aa@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (11, 'lol', 'lol', '$2a$10$.NG52cP4DXDgtQSd6I4aA.lne4F6QvNAo3muoxAN1IPrqi/RvrvPO', 'User', 'aasa@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (13, 'karate', 'karate111', '$2a$10$pXvz0IBM8UCw9/nx2J9hZecITFKc3ZWsMebNeJfd1eQIP3VnJHRsa', 'User', 'karate@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (14, 'alex', 'nikop', '$2a$10$VliwHAyi5uNYWRZMLaiU1.qCf4UkpiE4HXuhCbaIoh5BKh37rSwwi', 'User', 'alex@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (17, 'Kirill Ivanov', 'kirya123', '$2a$10$kEVkL01EHyti7zAq7GF8OujR.Vg/SrgltUiOBcgI2klQ2XKIlMrEG', 'User', 'ivanov.kir@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (18, 'NicerDicer', 'nicer', '$2a$10$QGqLdxHecuYJI08jp8aXVe9JGnx3CNzb0VmTb2MPqhkLlFf0N8IEq', 'User', 'aaab@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (22, 'menma', 'menma', '$2a$10$F9HfVkXJbzwvDP5Oh06n4e0kTUNPctaVHsyOTX1/p68S2fHVOxukC', 'User', 'naruto@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (23, 'Uzumaki Naruto', 'naruto', '$2a$10$Y7zotKlIVKvbKwkHaqBo.eLvKpVnjfBME5d0MlasMaDgeltRW86Lm', 'User', 'narutokun@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (33, 'username', 'username', '$2a$10$vnNlcpFIEF6LMjkb2Ygu3.73xh7XLVlOgKdpl6CCaIQ0iJJxrKd0S', 'User', 'username@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (19, 'kirill', 'doofer', '$2a$10$TyzpXHESdFBCn1sByj1aj.n4MCycuEQSc4C5dEzlNARVUZE28gimu', 'User', 'kirya@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (20, 'bug', 'scum', '$2a$10$zwrDd4zz1GLfyercHcJ2vedXrX2mhjN53taHwvCFrBJHMBHUJFpYC', 'User', 'bugsbunny@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (10, 'Olexij', 'alexij', '$2a$10$LwK0HLIdsGajk5D1p0fgJOc.oOofcfocXIhMy8TLWoy7K.R1bFc1W', 'Admin', 'ba@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (31, 'asd', 'asd', '$2a$10$8KD2jatZjMZXh6HyqGruW.F3i10ctMUYJept2yqT9isINrm22GMUq', 'User', 'asd@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (30, 'eminem', 'eminem', '$2a$10$kYrSZaDyHHk/6Y2BeGyYwuuXrNHkTrkufZfngEeR02f0gQ8GRE/ke', 'User', 'eminem@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (32, 'nikinemko', 'nikinemko', '$2a$10$oeC8Zzd9z3.smdYXZZFMWuYncFES3JRLrnbTikm8rCMKUXVGMJKEG', 'User', 'trilax@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (34, 'correctUsername', 'correctLogin', '$2a$10$oERW.x3dsnHbVw/z0QmXPuSCat1oisELFNWOkXyrL9DtP2asrInKG', 'User', 'correctemail@gmail.com', true);
INSERT INTO public.users (id, name, login, password, role, email, is_active) VALUES (9, 'al', 'al', '$2a$10$BdAg7Wp.k3TPUk694SzhBeEbWHEdrKLlvMfE4K.KyHBykKrYOVhpm', 'User', 'aaa@gmail.com', true);


--
-- Name: articles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.articles_id_seq', 47, true);


--
-- Name: auto_id_users; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auto_id_users', 3, true);


--
-- Name: tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tags_id_seq', 7, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 34, true);


--
-- Name: tags tags_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: users users_name_login_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_name_login_email_key UNIQUE (name, login, email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: fki_author_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_author_fkey ON public.articles USING btree (id);


--
-- Name: articles author_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT author_fkey FOREIGN KEY (author_id) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--

